import json
import pymxs #MaxScript Wrapper  
from pymxs import runtime as rt 



filemat=open(cache_path+"m2m.txt", 'r') #Set to your temp folder
matId=int(filemat.read())
filemat.close()
    
print(matId)


filemtl=open(cache_path+"GetMtl.txt", 'r') #Set to your temp folder
Base_MTL_Load=str(filemtl.read())
filemtl.close()
Base_MTL_Load = Base_MTL_Load.replace("\n",'')
Base_MTL_Load = Base_MTL_Load.replace('\\', '/')
Base_MTL_Load = Base_MTL_Load.replace('"', '')

print(Base_MTL_Load)



SC_Base = json.load(open(Base_MTL_Load))


#Name
Base_Name=(SC_Base ['Material']['SubMaterials']['Material'][matId].get('@Name'))

#Pull Spec information
Base_SpecularColor=(SC_Base ['Material']['SubMaterials']['Material'][matId].get('@Specular'))
Base_SpecRGB_STR=Base_SpecularColor.split(",")
Base_SpecR=Base_SpecRGB_STR[0]
Base_SpecG=Base_SpecRGB_STR[1]
Base_SpecB=Base_SpecRGB_STR[2]

try:
    Glow= (SC_Base ['Material']['SubMaterials']['Material'][matId].get('@Glow')) 
except KeyError:
    Glow=None

try:
    IllumColor= (SC_Base ['Material']['SubMaterials']['Material'][matId].get('@Emissive')) 
except KeyError:
    IllumColor=None
    
if not IllumColor == None:    
    ERGB_STR=IllumColor.split(",")
    IllumR=ERGB_STR[0]
    IllumG=ERGB_STR[1]
    IllumB=ERGB_STR[2]
else:
    IllumR=None
    IllumG=None
    IllumB=None

try:
    GlassTintC= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@TintColor')) 
except KeyError:
    GlassTintC=None
    
if not GlassTintC == None:    
    Glass_RGB_STR=GlassTintC.split(",")
    Glass_R=Glass_RGB_STR[0]
    Glass_G=Glass_RGB_STR[1]
    Glass_B=Glass_RGB_STR[2]
else:
    Glass_R=None
    Glass_G=None
    Glass_B=None
    
try:
    Base2_SpecularColor= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendLayer2SpecularColor')) 
except KeyError:
    Base2_SpecularColor=None
    
if not Base2_SpecularColor == None:    
    Base2_SpecRGB_STR=Base2_SpecularColor.split(",")
    Base2_SpecR=Base2_SpecRGB_STR[0]
    Base2_SpecG=Base2_SpecRGB_STR[1]
    Base2_SpecB=Base2_SpecRGB_STR[2]
else:
    Base2_SpecR=None
    Base2_SpecG=None
    Base2_SpecB=None
#Pull Diffuse

try:
    Base_DiffColor= (SC_Base  ['Material']['SubMaterials']['Material'][matId]['@Diffuse'])
except KeyError:
    Base_DiffColor=None

if not Base_DiffColor == None:
    Base_DiffRGB_STR=Base_DiffColor.split(",")
    Base_DiffR=Base_DiffRGB_STR[0]
    Base_DiffG=Base_DiffRGB_STR[1]
    Base_DiffB=Base_DiffRGB_STR[2]
else:
    Base_DiffR=None
    Base_DiffG=None
    Base_DiffB=None
try:
    SSS_amount= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@SSSIndex')) 
except KeyError:
    SSS_amount=None
try:
    Base2_DiffColor= (SC_Base  ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendLayer2DiffuseColor'))
except KeyError:
    Base2_DiffColor=None
    
if not Base2_DiffColor == None:
    Base2_DiffRGB_STR=Base2_DiffColor.split(",")
    Base2_DiffR=Base2_DiffRGB_STR[0]
    Base2_DiffG=Base2_DiffRGB_STR[1]
    Base2_DiffB=Base2_DiffRGB_STR[2]
else:
    Base2_DiffR=None
    Base2_DiffG=None
    Base2_DiffB=None
    
try:
    TintCloudiness=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@TintCloudines'))
except:
    TintCloudiness=None

    #BlendMask
try:
    Base_BlendLayerTile=(SC_Base  ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendMaskTiling'))
except:
    Base_BlendLayerTile=None
try:
    Base_WearBlendBase=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@WearBlendBase'))
except:
   Base_WearBlendBase=None 
try:
    Base_BlendFactor=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendFactor'))
except:
    Base_BlendFactor=None
#Pull Gloss
try:
    Base_Gloss= (SC_Base ['Material']['SubMaterials']['Material'][matId].get('@Shininess'))
except:
    Base_Gloss=None
try:
    Base2_Gloss= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendLayer2Glossiness'))
except:
    Base2_Gloss=None
    #Pull Detail Tile
try:
    Base_Tile=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@MacroTiling'))
except:
    Base_Tile=None
    #Detail Diffuse
try:
    Base_DetaiBase_Diff=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@DetailDiffuseScale'))
except:
    Base_DetaiBase_Diff=None
try:
    BaseB_DetaiBase_Diff=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendDetailDiffuseScale'))
except:
    BaseB_DetaiBase_Diff=None    
    #Detail Normal
try:
    Base_Detail_Nrm=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendDetailBumpScale'))
except:
    Base_Detail_Nrm=None
try:
    BaseB_Detail_Nrm=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@DetailBump'))
except:
    BaseB_Detail_Nrm=None
#Detail Gloss
try:
    Base_Detail_Gloss=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('DetailGlossScale'))
except:
    Base_Detail_Gloss=None
try:
    BaseB_Detail_Gloss=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendDetailGlossScale'))
except:
    BaseB_Detail_Gloss=None

try:
    DisplPOM=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@PomDisplacement'))
except:
    DisplPOM=None

#baseblendLayer
def search_texmap (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['Textures']['Texture']):
  if name.lower() == keyval['@Map'].lower():
   return keyval['@File']

# MatLayer
def search_layer (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@Path']

   
def tint_layer (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@TintColor']

   
def tint_GlossMult (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@GlossMult']

   
def tint_UVtile (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@UVTiling']

def tint_pallete (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@PaletteTint']
   
def U_Tilez (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Map'].lower():
   return keyval['@TileU']
   
def V_Tilez (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Map'].lower():
   return keyval['@TileV']
   
def U_offset (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['Textures']['Texture']):
  if name.lower() == keyval['@Map'].lower():
   return keyval['TexMod']['@OffsetU']
   
def V_offset (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['Textures']['Texture']):
  if name.lower() == keyval['@Map'].lower():
   return keyval['TexMod']['@OffsetV']


try:
    Base_TextMapDiff=search_texmap ('Diffuse')
except KeyError:
    Base_TextMapDiff=None
except TypeError:
    Base_TextMapDiff=SC_Base ['Material']['SubMaterials']['Material'][matId]['Textures']['Texture']['@File']
    
try:
    Base_TextMapNormal=search_texmap ('Bumpmap')
except (KeyError,TypeError):
    Base_TextMapNormal=None

try:
    Base_TextMapDetail=search_texmap ('Detail')
except (KeyError,TypeError):
    Base_TextMapDetail=None
try:
    Base_TextMapSpec=search_texmap ('Specular')
except (KeyError,TypeError):
    Base_TextMapSpec=None
    
try:
    BaseB_TextMapDiff=search_texmap ('Decal')
except (KeyError,TypeError):
    BaseB_TextMapDiff=None
try:
    Base_TextMapDispl=search_texmap ('Heightmap')
except (KeyError,TypeError):
    Base_TextMapDispl=None
try:
    BaseB_TextMapNormal=search_texmap ('Custom')
except (KeyError,TypeError):
    BaseB_TextMapNormal=None
try:
    BaseB_TextMapDetail=search_texmap ('BlendDetail')
except (KeyError,TypeError):
    BaseB_TextMapDetail=None
try:
    BaseB_TextMapSpec=search_texmap ('SubSurface')
except (KeyError,TypeError):
    BaseB_TextMapSpec=None
try:
    BaseB_TextMapMask=search_texmap ('Opacity')
except (KeyError,TypeError):
    BaseB_TextMapMask=None

try:
    BaseB_TextMapSSS=search_texmap ('SubSurfaceScatter')
except (KeyError,TypeError):
    BaseB_TextMapSSS=None
    
try:
    Layer1mtl=search_layer ('Primary')

except KeyError:
    Layer1mtl=None
    
except TypeError:
    Layer1mtl=SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']['@Path']
    
try:
    Layer2mtl=search_layer ('Wear')
except (KeyError, TypeError):
    Layer2mtl=None
    
try:
    Layer1amtl=search_layer ('Secondary')
except (KeyError, TypeError):
    Layer1amtl=None

try:
    TintPallete_Base=tint_pallete ('Primary')
except (KeyError, TypeError):
    TintPallete_Base=None
    
try:
    TintPallete_Blend=tint_pallete ('Wear')
except (KeyError, TypeError):
    TintPallete_Blend=None

try:
    TintPallete_PWS=tint_pallete ('Secondary')
except (KeyError, TypeError):
    TintPallete_PWS=None
    

#Layer 1 information
try:
    Base_TintColor1=tint_layer ('Primary')
except KeyError:
    Base_TintColor1=None
except TypeError:
    Base_TintColor1=SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']['@TintColor']
    
if not Base_TintColor1== None:
    Base_TintColor1_str=Base_TintColor1.split(",")
    Base_TintR=Base_TintColor1_str[0]
    Base_TintG=Base_TintColor1_str[1]
    Base_TintB=Base_TintColor1_str[2]
else:
    Base_TintR=None
    Base_TintG=None
    Base_TintB=None

#Layer 2 information
try:
    Base_TintColor2=tint_layer ('Wear')
except (KeyError, TypeError):
    Base_TintColor2=None
    
if not Base_TintColor2 ==None:
    Base_TintColor2_str=Base_TintColor2.split(",")
    Base2_TintR=Base_TintColor2_str[0]
    Base2_TintG=Base_TintColor2_str[1]
    Base2_TintB=Base_TintColor2_str[2]
else:
    Base2_TintR=None
    Base2_TintG=None
    Base2_TintB=None

try:
    SSS_Color= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@TransmittanceColor')) 
except KeyError:
    SSS_Color=None
    
if not SSS_Color == None:    
    Base2_SpecRGB_STR=Base2_SpecularColor.split(",")
    SSS_R=Base2_SpecRGB_STR[0]
    SSS_G=Base2_SpecRGB_STR[1]
    SSS_B=Base2_SpecRGB_STR[2]
else:
    SSS_R=None
    SSS_G=None
    SSS_B=None
    
try: 
    #Layer 1 Gloss
    Layer1Gloss=tint_GlossMult ('Primary')

except KeyError:
    Layer1Gloss=None
except TypeError:
    Layer1Gloss=SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']['@GlossMult']
try: 
    Layer2Gloss=tint_GlossMult ('Wear')

except (KeyError, TypeError):

    Layer2Gloss=None
    #Layer Gloss 1a   
try: 
    Layer1aGloss=tint_GlossMult ('Secondary')
except (KeyError, TypeError):

    Layer1aGloss=None
    # TILLING TILLING  TILLING TILLING  TILLING TILLING  TILLING TILLING  TILLING TILLING  TILLING TILLING  TILLING TILLING 


try: 
    Layer1UV=tint_UVtile ('Primary')
except KeyError:
    Layer1UV=None
except TypeError:
     Layer1UV=SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']['@UVTiling']
try: 
    Layer2UV=tint_UVtile ('Wear')
except (KeyError, TypeError):
    Layer2UV=None
try: 
    Layer1aUV=tint_UVtile ('Secondary')
except (KeyError, TypeError):
    Layer1aUV=None
 #Detail Tile
try: 
    Base_Detail_TileU=U_Tilez ('Detail')
except (KeyError, TypeError):
    Base_Detail_TileU="8" 
    
try: 
    Base_Detail_TileV=V_Tilez ('Detail')
except (KeyError, TypeError):
    Base_Detail_TileV="8" 
    
try: 
    Blend_Detail_TileU=U_Tilez ('BlendDetail')
except (KeyError, TypeError):
    Blend_Detail_TileU="8" 
    
try: 
    Blend_Detail_TileV=V_Tilez ('BlendDetail')

except (KeyError, TypeError):
    Blend_Detail_TileV="8"  
    
try:
    Main_TileU=U_Tilez ('Diffuse')
except (KeyError, TypeError):
    Main_TileU="1" 
    
try:
    Main_TileV=V_Tilez ('Diffuse')
except (KeyError, TypeError):
    Main_TileV="1" 
    
    
try:
    Main_OFFU=U_offset ('Diffuse')
except (KeyError, TypeError):
    Main_OFFU="0" 
    
try:
    Main_OFFV=V_offset ('Diffuse')
except (KeyError, TypeError):
    Main_OFFV="0" 
    
    
try:
    BMain_TileU=U_Tilez ('Decal')
except (KeyError, TypeError):
    BMain_TileU="1" 
    
try:
    BMain_TileV=V_Tilez ('Decal')
except (KeyError, TypeError):
    BMain_TileV="1" 
    
try:
    BMain_OFFU=U_offset ('Decal')
except (KeyError, TypeError):
    BMain_OFFU="0" 
    
try:
    BMain_OFFV=V_offset ('Decal')
except (KeyError, TypeError):
    BMain_OFFV="0" 
    
#DDNAGLOSS
try:
    Gloss_TileU=U_Tilez ('Bumpmap')
except (KeyError, TypeError):
    Gloss_TileU="1" 
    
try:
    Gloss_TileV=V_Tilez ('Bumpmap')
except (KeyError, TypeError):
    Gloss_TileV="1" 
    
try:
    Gloss_OFFU=U_offset ('Bumpmap')
except (KeyError, TypeError):
    Gloss_OFFU="0" 
    
try:
    Gloss_OFFV=V_offset ('Bumpmap')
except (KeyError, TypeError):
    Gloss_OFFV="0" 
    
    
try:
    BGloss_TileU=U_Tilez ('Custom')
except (KeyError, TypeError):
    BGloss_TileU="1" 
    
try:
    BGloss_TileV=V_Tilez ('Custom')
except (KeyError, TypeError):
    BGloss_TileV="1" 
    
try:
    BGloss_OFFU=U_offset ('Custom')
except (KeyError, TypeError):
    BGloss_OFFU="0" 
    
try:
    BGloss_OFFV=V_offset ('Custom')
except (KeyError, TypeError):
    BGloss_OFFV="0" 
    
#detail offset
try:
    Detail_OFFU=U_offset ('Detail')
except (KeyError, TypeError):
    Detail_OFFU="0" 
    
try:
    Detail_OFFV=V_offset ('Detail')
except (KeyError, TypeError):
    Detail_OFFV="0" 
    
try:
    BDetail_OFFU=U_offset ('BlendDetail')
except (KeyError, TypeError):
    BDetail_OFFU="0" 
    
try:
    BDetail_OFFV=V_offset ('BlendDetail')
except (KeyError, TypeError):
    BDetail_OFFV="0" 

ZExport=[
'mtlname="',(Base_Name or ""),'"\n',
'Base_diff="',(Base_TextMapDiff or ""),'"\n',
'Base_ddna="',(Base_TextMapNormal or ""),'"\n',
'Base_detail="',(Base_TextMapDetail or ""),'"\n',
'Base_spec="',(Base_TextMapSpec or ""),'"\n',
'BaseB_diff="',(BaseB_TextMapDiff or ""),'"\n',
'Base_disp="',(Base_TextMapDispl or ""),'"\n',
'BaseB_ddna="',(BaseB_TextMapNormal or ""),'"\n',
'BaseB_detail="',(BaseB_TextMapDetail or ""),'"\n',
'BaseB_spec="',(BaseB_TextMapSpec or ""),'"\n',
'Base_mask="',(BaseB_TextMapMask or ""),'"\n',
'Base_SpecR="',(Base_SpecR or ""),'"\n',
'Base_SpecG="',(Base_SpecG or ""),'"\n',
'Base_SpecB="',(Base_SpecB or ""),'"\n',
'BaseB_SpecR="',(Base2_SpecR or ""),'"\n',
'BaseB_SpecG="',(Base2_SpecG or ""),'"\n',
'BaseB_SpecB="',(Base2_SpecB or ""),'"\n',
'Base_DiffR="',(Base_DiffR or ""),'"\n',
'Base_DiffG="',(Base_DiffG or ""),'"\n',
'Base_DiffB="',(Base_DiffB or ""),'"\n',
'Base2_DiffR="',(Base2_DiffR or ""),'"\n',
'Base2_DiffG="',(Base2_DiffG or ""),'"\n',
'Base2_DiffB="',(Base2_DiffB or ""),'"\n',
'Base_BlendLayerTile="',(Base_BlendLayerTile or ""),'"\n',
'Base_WearBlendBase="',(Base_WearBlendBase or ""),'"\n',
'Base_BlendFactor="',(Base_BlendFactor or ""),'"\n',
'Base_Gloss="',(Base_Gloss or ""),'"\n',
'Base2_Gloss="',(Base2_Gloss or ""),'"\n',
'Base_Tile="',(Base_Tile or ""),'"\n',
'Base_Detail_DiffScale="',(Base_DetaiBase_Diff or ""),'"\n',
'Base2_Detail_DiffScale="',(BaseB_DetaiBase_Diff or ""),'"\n',
'Base_Detail_Nrm="',(Base_Detail_Nrm or ""),'"\n',
'BaseB_Detail_Nrm="',(BaseB_Detail_Nrm or ""),'"\n',
'Base_Detail_Gloss="',(Base_Detail_Gloss or ""),'"\n',
'BaseB_Detail_Gloss="',(BaseB_Detail_Gloss or ""),'"\n',
'Base_TintR="',(Base_TintR or ""),'"\n',
'Base_TintG="',(Base_TintG or ""),'"\n',
'Base_TintB="',(Base_TintB or ""),'"\n',
'Base2_TintR="',(Base2_TintR or ""),'"\n',
'Base2_TintG="',(Base2_TintG or ""),'"\n',
'Base2_TintB="',(Base2_TintB or ""),'"\n',
'Base_LayerGloss="',(Layer1Gloss or ""),'"\n',
'Base2_LayerGloss="',(Layer2Gloss or ""),'"\n',
'Base_Layer1UV="',(Layer1UV or "1"),'"\n',
'Base2_Layer2UV="',(Layer2UV or "1"),'"\n',
'LayerTF2="',(Layer2mtl or ""),'"\n',
'LayerTF1="',(Layer1mtl or ""),'"\n',
'DISP="',(DisplPOM or ""),'"\n',
'TintPallete_Base="',(TintPallete_Base or ""),'"\n',
'TintPallete_Blend="',(TintPallete_Blend or ""),'"\n',
'Base_Detail_TileU="',(Base_Detail_TileU or ""),'"\n',
'Base_Detail_TileV="',(Base_Detail_TileV or ""),'"\n',
'Blend_Detail_TileU="',(Blend_Detail_TileU or ""),'"\n',
'Blend_Detail_TileV="',(Blend_Detail_TileV or ""),'"\n',
'IllumR="',(IllumR or ""),'"\n',
'IllumG="',(IllumG or ""),'"\n',
'IllumB="',(IllumB or ""),'"\n',
'Glow="',(Glow or ""),'"\n',
'Glass_R="',(Glass_R or ""),'"\n',
'Glass_G="',(Glass_G or ""),'"\n',
'Glass_B="',(Glass_B or ""),'"\n',
'TintCloudiness="',(TintCloudiness or ""),'"\n',
'Main_TileU="',(Main_TileU or ""),'"\n',
'Main_TileV="',(Main_TileV or ""),'"\n',
'Main_OFFU="',(Main_OFFU or ""),'"\n',
'Main_OFFV="',(Main_OFFV or ""),'"\n',
'BMain_TileU="',(BMain_TileU or ""),'"\n',
'BMain_TileV="',(BMain_TileV or ""),'"\n',
'BMain_OFFU="',(BMain_OFFU or ""),'"\n',
'BMain_OFFV="',(BMain_OFFV or ""),'"\n',
'Gloss_TileU="',(Gloss_TileU or ""),'"\n',
'Gloss_TileV="',(Gloss_TileV or ""),'"\n',
'Gloss_OFFU="',(Gloss_OFFU or ""),'"\n',
'Gloss_OFFV="',(Gloss_OFFV or ""),'"\n',
'BGloss_TileU="',(BGloss_TileU or ""),'"\n',
'BGloss_TileV="',(BGloss_TileV or ""),'"\n',
'BGloss_OFFU="',(BGloss_OFFU or ""),'"\n',
'BGloss_OFFV="',(BGloss_OFFV or ""),'"\n',
'Detail_OFFU="',(Detail_OFFU or ""),'"\n',
'Detail_OFFV="',(Detail_OFFV or ""),'"\n',
'BDetail_OFFU="',(BDetail_OFFU or ""),'"\n',
'BDetail_OFFV="',(BDetail_OFFV or ""),'"\n',
'SSSTexMap="',(BaseB_TextMapSSS or ""),'"\n',
'SSSAmount="',(SSS_amount or "1"),'"\n',
'SSS_R="',(SSS_R or ""),'"\n',
'SSS_G="',(SSS_G or ""),'"\n',
'SSS_B="',(SSS_B or ""),'"\n',
'Camo1_DiffR="',(""),'"\n',
'Camo1_DiffG="',(""),'"\n',
'Camo1_DiffB="',(""),'"\n',
'Camo2_DiffR="',(""),'"\n',
'Camo2_DiffG="',(""),'"\n',
'Camo2_DiffB="',(""),'"\n',
'Camo3_DiffR="',(""),'"\n',
'Camo3_DiffG="',(""),'"\n',
'Camo3_DiffB="',(""),'"\n',
'Stencil_Tile="',("7"),'"\n',
'StencilOpacity="',(""),'"\n',
'StencilMask_TEX="',(""),'"\n',
'LayerTFS="',(Layer1amtl or ""),'"\n',
'TintPallete_PWS="',(TintPallete_PWS or ""),'"\n',
'PWS_Detail_TileU="',(Layer1aUV or "1"),'"\n',
'PWS_Detail_TileV="',(Layer1aUV or "1"),'"\n',
'PWS_LayerGloss="',(Layer1aGloss or "1"),'"\n',
'PWS_TINT_R="',(Base1a_TintR or ""),'"\n',
'PWS_TINT_G="',(Base1a_TintG or ""),'"\n',
'PWS_TINT_B="',(Base1a_TintB or ""),'"\n',
'PWS_TileU="',(Layer1aUV or "1"),'"\n',
'PWS_TileV="',(Layer1aUV or "1"),'"\n',
]


Layerz_Input=[Layer1mtl or ""]

LayerX_Input=[Layer2mtl or ""]

LayerPSW_Input=[Layer1amtl or ""]

file1=open(cache_path+'BaseOutput.txt', 'w')#Set to your temp folder
file1.writelines(ZExport)
file1.close()

file2=open(cache_path+'Layer1Input.txt', 'w')#Set to your temp folder
file2.writelines(Layerz_Input)
file2.close()

file3=open(cache_path+'Layer2Input.txt', 'w')#Set to your temp folder
file3.writelines(LayerX_Input)
file3.close()

file4=open(cache_path+'Layer1aInput.txt', 'w')#Set to your temp folder
file4.writelines(LayerPSW_Input)
file4.close()

filemtl.close()

gc.collect()