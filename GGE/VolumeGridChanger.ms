try (closeRolloutFloater VrayVolumeMasterController2) catch()




rollout VrayVolumeMasterController2 "VRayVolumeGrid Master Fixer" width:200 height:280
(

	
	button 'ReplacePath' "Change Path " pos:[20,50] width:150 height:40 align:#left
	button 'GlobalGPU' "GPU Preview " pos:[20,120] width:150 height:40 align:#left


	
	on ReplacePath pressed do
	(
			for obj in geometry where classof obj == VRayVolumeGrid do
			(
				-- Check if the .inpathraw contains "M:\"
				if findString obj.inpathraw "M:\\" != undefined do
				(
					-- Replace "M:\" with "D:\"
					obj.inpathraw = substituteString obj.inpathraw "M:\\" "D:\\"
				)
			)	

	)
	
	
	on GlobalGPU pressed do
	(
			for obj in geometry where classof obj == VRayVolumeGrid do
			(
					obj.gpu_viewport=on 
			)	

	)
	
)


createDialog VrayVolumeMasterController2