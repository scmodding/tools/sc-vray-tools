try (closeRolloutFloater EnviromentalLayers) catch()

rollout SC_EnvirLayerSetup "Enviroment Layer Setup" width:500 height:200
(


	button 'CreateLayer' "Create Layers" pos:[180,139] width:150 height:33 toolTip:"Creates new layers to help organize enviroments" align:#left
	editText 'SpecialName' "Name" pos:[37,76] width:385 height:20 align:#left text:"Envir"
	
	on CreateLayer pressed do
	(
		LayerManager.newLayerFromName (SpecialName.text + "_Interior Lights")	
		LayerManager.newLayerFromName (SpecialName.text + "_INT_LIGHTS_STD")		
		LayerManager.newLayerFromName (SpecialName.text + "_INT_LIGHTS_STD")
		LayerManager.newLayerFromName (SpecialName.text + "_INT_LIGHTS_AUX")
		LayerManager.newLayerFromName (SpecialName.text + "_INT_LIGHTS_EMG")
		LayerManager.newLayerFromName (SpecialName.text + "_Exterior Lights")
		LayerManager.newLayerFromName (SpecialName.text + "_EXT_Manmade Lights")
		LayerManager.newLayerFromName (SpecialName.text + "_EXT_Natural Lights")
		LayerManager.newLayerFromName (SpecialName.text + "_LIGHTS")		
		LayerManager.newLayerFromName (SpecialName.text + "_Helpers")
		LayerManager.newLayerFromName (SpecialName.text + "_Main Stage")
		LayerManager.newLayerFromName (SpecialName.text + "_Root")
		LayerManager.newLayerFromName (SpecialName.text + "_Skybox")
		LayerManager.newLayerFromName (SpecialName.text + "_Vista")			
	)
	
)

CreateDialog SC_EnvirLayerSetup