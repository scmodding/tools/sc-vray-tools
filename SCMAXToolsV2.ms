
rollout page1Rollout "Material"
(
    label lblPage1 "Tools for Material Creation"
    button Load_MTL "General MTL Loader [A]"  tooltip:"This is your main material creator"
	button Load_WDAE "WDA BW [A]"  tooltip:"Used to older handle WDA material. Used for Star Citizen Build 3.xx and older "
	button Load_WDAEV2 "WDA V2 [A]" tooltip:"Use for current WDA material. Used for Star Citizen Build 3.xx and newer "
	button Load_WDAC "WDA Clothing [A]"  tooltip:"Used to older handle WDA material related to Clothing, Armor, and some Random Buildset items. Used for Star Citizen Build 3.xx and older "
	button Load_Layer "Layer Loader (WDA)" tooltip:"For Creating the NEED material layer for WDA files."
	
	
	
	on Load_MTL pressed do
	(
		fileIn("StarFabPython/SC_MTL_Loader_V4.ms")
	)
	
	on Load_WDAE pressed do
	(
		fileIn("StarFabPython/SC_WDA_Equipment_Importer.ms")
	)

	
	on Load_WDAEV2 pressed do
	(
		filein ("StarFabPython/SC_WDA_Equipment_Importer_V2_WEAR.ms")
	)
	
	on Load_WDAC pressed do
	(
		filein ("StarFabPython/SC_WDA_Clothing_Importer.ms")
	)
	
	on Load_Layer pressed do
	(
		fileIn("StarFabPython/SC_Layer_Importer.ms")
	)
	
	
	
)

rollout page2Rollout "Scene Setup and Rigging"
(
    label lblPage2 "Tools for Scene Setup and Rigging"
	
	button Load_Tint "Tint Loader" tooltip:"Give your Ship some Colors"
	button Quad_btn "Quadmaster" tooltip:"Fix the weight issues and quadfiy meshes and make them pink"
	button layersetup_btn "Layer Setup" tooltip:"Layers to help organize the chaos" 
	button ControlCube "WD Control Cube" tooltip:"Use the Material on this Cube to change how dirty or worn your model or scene is"
	button Ship_btn "Ship Controller" tooltip:"Place to put your Animation Reaction Controls"
	button Root_btn "Root Maker" tooltip:"Makes the selected object have a Dummy to act as Root"
	button AnimRect_Ctrl_btn "Animation Controller" tooltip:"Makes a square spline that you can use to help control Animation Reactions"
	button Load_Suffix "Suffix Modder" tooltip:"Changes the suffix on the selected material in the material editor"
    button Load_ScreenModderMass "Mass Suffix Renamer" tooltip:"Changes the suffix of ALL Materials in the Scene"
	
		on Load_ScreenModderMass pressed do
		(
			filein ("StarFabPython/SC_MassSuffix_Renamer.ms")
		)
	
			on Load_Suffix pressed do
			(
				filein ("StarFabPython/SC_SuffixModder.ms")
			)
			
		on layersetup_btn pressed do
		(
			filein ("SC_ShipLayers.ms")
		)
		
		on ControlCube pressed do
		(
			
			filein ("StarFabPython/SC_WD_ControlCube.ms")
		)
		
			on Quad_btn pressed do
		(
			
			filein ("QUADMASTER.ms")
		)

		on Load_Tint pressed do
			(
				fileIn("StarFabPython/SC_TintLoader.ms")
			)
		
		on Ship_btn pressed do
		(
				fileName = getFilenameFile maxFileName
				Star radius1:80 radius2:30 fillet1:0 fillet2:0 numPoints:3 distort:0 pos:[0,0,0] isSelected:on
				modPanel.addModToSelection (EmptyModifier ()) ui:on	
				$.name=filename+"_Master Controller"
				$.wirecolor = color 87 225 87
				rotate $ (angleaxis -90 [0,0,1])
			
		)
			
		on AnimRect_Ctrl_btn pressed do
			
		(
				Rectangle length:119.163 width:59.8064 cornerRadius:0 pos:[0,0,0] isSelected:on
				modPanel.addModToSelection (EmptyModifier ()) ui:on	
				$.name="Anim Ctrlr_"
				$.wirecolor = color 87 225 87
			
		)
		
		on Root_btn pressed do
			
		(
			for obj in 1 to selection.count do 
			(
			dummyObj = dummy()
			dummyObj.pos = selection[obj].pivot
			dummyObj.scale=[2,2,2]
			dummyObj.name= selection[obj].name + "_ROOT"
			dummyObj.parent = selection[obj].parent
			selection[obj].parent=dummyObj
			)
		
		)
	
)

rollout page3Rollout "Lights"
(
    label lblPage3 "Light Tools"
    button Load_LightVray "Light Loader"
	
	on Load_LightVray pressed do
		(
			fileIn("R:\Program Files\AutoDesk\3ds Max 2022\scripts\StarFabPython\SC_Light_load_V2_STD.ms")
		)

)

rollout page4Rollout "Other Tools"
(
    label lblPage5 "Tools to help do other things"
    button PathSettings "Settings"
	
	on PathSettings pressed do
		(
			filein ("StarFabPython/SF_Settings.ms")
		)

)

rollout page5Rollout "Fixing Tools"
(
    label lblPage4 "Tools for fixing your ***kups"
    button Load_Tintrenamer "Tint Renamer"
    button btn_dirtren "Dirt Renamer (EXT/INT)"
	
	
	
	on Load_Tintrenamer pressed do
		(
			fileIn("StarFabPython/ButtonScripts/TintNameAdjuster.ms")
		)
		
	on Load_Tintrenamer pressed do
	(
		fileIn("StarFabPython/ButtonScripts/TintNameAdjuster.ms")
	)
	
	on btn_dirtren pressed do
	(
		filein ("StarFabPython/SC_DirtRenamer.ms")
	)
)

rollout page6Rollout "Settings"
(
    label lblPage5 "Set Path Settings"
    button PathSettings "Settings"
	
	on PathSettings pressed do
		(
			filein ("StarFabPython/SF_Settings.ms")
		)

)



	
	
	--Main Script
	try (closeRolloutFloater SC_VRayToolsV2) catch()
	rollout SC_VRayToolsV2 "SC VRay Tools Alpha 0.4.0" width:600 height:629
	(
		bitmap 'bmp2' "Bitmap" pos:[0,0] width:600 height:131 align:#left filename:"StarFabPython/SCToolset.png"
		

		
		subRollout subRolloutContainer "Pages" width:570 height:500 
		
	on SC_VRayToolsV2 open do
    (
        addSubRollout subRolloutContainer page1Rollout 
        addSubRollout subRolloutContainer page2Rollout 
        addSubRollout subRolloutContainer page3Rollout 
		addSubRollout subRolloutContainer page3Rollout 
        addSubRollout subRolloutContainer page4Rollout 
        addSubRollout subRolloutContainer page5Rollout 
        addSubRollout subRolloutContainer page6Rollout 
       
    )

		
	)

	
		CreateDialog SC_VRayToolsV2
