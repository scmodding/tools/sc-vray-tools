import json
import pymxs #MaxScript Wrapper  
from pymxs import runtime as rt 

maxINI = rt.getMAXIniFile()
tex_path = rt.getINISetting(maxINI, "SCTools", "tex_path")
cache_path = rt.getINISetting(maxINI, "SCTools", "cache_path")
script_path = rt.getINISetting(maxINI, "SCTools", "script_path")
json_path = rt.getINISetting(maxINI, "SCTools", "json_path")

#Change the beginning of your directory to your JSON MTL location
with open (cache_path+"Layer1aInput.txt", 'r') as file:
    Layer1amtl=file.read()
    file.close()





LS_MTL_Load=open (json_path+Layer1amtl)#Insert your JSON directory

SC_LS = json.load(LS_MTL_Load)

#
try:
    Layer1a_Pat=(SC_LS ['Material']['Textures']['Texture'])
except KeyError:
    Layer1a_Pat=None
    

#Pull Spec information
try:
    LS_SpecularColor= (SC_LS ['Material']['@Specular'])
except KeyError:
    LS_SpecularColor=None

if not L_SpecularColor ==None:
    LS_SpecRGB_STR=L_SpecularColor.split(",")
    LS_SpecR=L_SpecRGB_STR[0]
    LS_SpecG=L_SpecRGB_STR[1]
    LS_SpecB=L_SpecRGB_STR[2]
else:
    LS_SpecR=None
    LS_SpecG=None
    LS_SpecB=None
    
#Pull Diffuse
try:
    LS_DiffColor= (SC_LS ['Material']['@Diffuse'])
except KeyError:
    LS_DiffColor=None
if not L_DiffColor==None:
    LS_DiffRGB_STR=L_DiffColor.split(",")
    LS_DiffR=L_DiffRGB_STR[0]
    LS_DiffG=L_DiffRGB_STR[1]
    LS_DiffB=L_DiffRGB_STR[2]
else:
    LS_DiffR=None
    LS_DiffG=None
    LS_DiffB=None
    
#Pull Gloss
try:
    LS_Gloss= (SC_LS ['Material']['@Shininess'])
except KeyError:
    LS_Gloss=None
#Pull Detail Tile
try:
    LS_DTile=(SC_LS ['Material']['PublicParams']['@DetailTiling'])
except KeyError:
    LS_DTile=None
#Detail Diffuse
try:
    LS_Detail_Diff=(SC_LS ['Material']['PublicParams']['@DetailDiffuse'])
except KeyError:
    LS_Detail_Diff=None
#Detail Normal
try:
    LS_Detail_Nrm=(SC_LS ['Material']['PublicParams']['@DetailBump'])
except KeyError:
    LS_Detail_Nrm=None
#Detail Gloss
try:
    LS_Detail_Gloss=(SC_LS ['Material']['PublicParams']['@DetailGloss'])
except KeyError:
    LS_Detail_Gloss=None
    
try:
    LS_Detail_Tile=(SC_LS ['Material']['PublicParams']['@DetailTiling'])
except KeyError:
    LS_Detail_Tile=None

try:
    LS_ClearCoat=(SC_LS ['Material']['PublicParams']['@ClearCoatBlend'])
except KeyError:
    LS_ClearCoat=None
    
    
def search_texmap (name):
 for keyval in Layer1a_Pat:
  if name.lower() == keyval['@Map'].lower():
   return keyval['@File']

try:
    LS_TextMapDiff=search_texmap ('TexSlot1')
except (KeyError,TypeError):
    LS_TextMapDiff=None
try:
    LS_TextMapNormal=search_texmap ('TexSlot2')
except (KeyError,TypeError):
    LS_TextMapNormal=None
try:
    LS_TextMapDetail=search_texmap ('TexSlot7')
except (KeyError,TypeError):
    LS_TextMapDetail=None
try:
    LS_TextMapSpec=search_texmap ('TexSlot6')
except (KeyError,TypeError):
    LS_TextMapSpec=None
    
try:
    LS_TextMapDisp=search_texmap ('TexSlot3')
except (KeyError,TypeError):
    LS_TextMapDisp=None

print('Layer_1a_Diff="',(LS_TextMapDiff or ""),'"\n',
'Layer_1a_Normal="',(LS_TextMapNormal or ""),'"\n',
'Layer_1a_Detail="',(LS_TextMapDetail or ""),'"\n',
'Layer_1a_SpecR=',(LS_SpecR or ""),'\n',
'Layer_1a_SpecG=',(LS_SpecG or ""),'\n',
'Layer_1a_SpecB=',(LS_SpecB or ""),'\n',
'Layer_1a_DiffR=',(LS_DiffR or ""),'\n',
'Layer_1a_DiffG=',(LS_DiffG or ""),'\n',
'Layer_1a_DiffB=',(LS_DiffB or ""),'\n',
'Layer_1a_Gloss=',(LS_Gloss or ""),'\n',
'Layer_1a_Detail_diff_Scale=',(LS_Detail_Diff or ""),'\n',
'Layer_1a_Detail_ddna_Scale=',(LS_Detail_Nrm or ""),'\n',
'Layer_1a_Detail_gloss_Scale=',(LS_Detail_Gloss or ""),'\n',
'Layer_1a_Spec="',(LS_TextMapSpec or ""),'"\n',
'Layer_1a_Dtile=',(LS_Detail_Tile or ""),'\n',
'Layer_1a_CC="',(LS_ClearCoat or ""),'"\n',)


ZExport=['Layer_1a_Diff="',(LS_TextMapDiff or ""),'"\n',
'Layer_1a_Normal="',(LS_TextMapNormal or ""),'"\n',
'Layer_1a_Detail="',(LS_TextMapDetail or ""),'"\n',
'Layer_1a_SpecR=',(LS_SpecR or ""),'\n',
'Layer_1a_SpecG=',(LS_SpecG or ""),'\n',
'Layer_1a_SpecB=',(LS_SpecB or ""),'\n',
'Layer_1a_DiffR=',(LS_DiffR or ""),'\n',
'Layer_1a_DiffG=',(LS_DiffG or ""),'\n',
'Layer_1a_DiffB=',(LS_DiffB or ""),'\n',
'Layer_1a_Gloss=',(LS_Gloss or ""),'\n',
'Layer_1a_Detail_diff_Scale=',(LS_Detail_Diff or ""),'\n',
'Layer_1a_Detail_ddna_Scale=',(LS_Detail_Nrm or ""),'\n',
'Layer_1a_Detail_gloss_Scale=',(LS_Detail_Gloss or ""),'\n',
'Layer_1a_Spec="',(LS_TextMapSpec or ""),'"\n',
'Layer_1a_Dtile=',(LS_Detail_Tile or ""),'\n',
'Layer_1a_CC="',(LS_ClearCoat or ""),'"\n',
'Layer_1a_Displ="',(LS_TextMapDisp or ""),'"\n',
]


Layer3Output = open(cache_path+'Layer1aOutput.txt', 'w')#Change to your local temp 
Layer3Output.writelines(ZExport)
Layer3Output.close()

LS_MTL_Load.close()

gc.collect()
