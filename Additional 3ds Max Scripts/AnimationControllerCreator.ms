rollout Anim_Controller_Maker "Animation Controller Maker" width:500 height:280
	
(

		button 'MakeSpline' "Make" pos:[350,40] width:120 height:120 align:#left border:true 
		groupBox 'grp21' "Parameters" pos:[20,5] width:320 height:180 align:#left
		editText 'CRTL_Name' "Name" pos:[28,30] width:285 height:20 align:#left Text: "Generic"
		editText 'Animation_Name' "Animation Name" pos:[28,55] width:285 height:20 align:#left Text: "Animation"
	
		groupBox 'grp21Size' "Size" pos:[65,85] width:230 height:40 align:#left
		spinner 'X_Size' "Length" pos:[80,100] width:70 height:50 align:#left range:[0,1000.0,50.0] scale: 0.25
		spinner 'Y_Size' "Width" pos:[180,100] width:70 height:50 align:#left range:[0,1000.0,50.0] scale: 0.25
	
		spinner 'TimeScaleMax' "Time Scale Max" pos:[100,140] width:70 height:50 align:#left range:[0,1000,30.0] scale: 1.0
		spinner 'TimeScaleMin' "Time Scale Min" pos:[100,160] width:70 height:50 align:#left range:[-1000,0,0.0] scale: 1.0
		checkbox 'SyncBox' "Sync Box" pos:[220,140] width:80 height:18 align:#left checked:off
	
	
		groupBox 'grp21_preset' "Presets" pos:[20,190] width:320 height:85 align:#left
		button 'PreSet_DOOR' "DOOR" pos:[30,210] width:80 height:25 align:#left border:true 
		button 'PreSet_RESET' "RESET" pos:[30,240] width:82 height:27 align:#left border:true 		
		button 'PreSet_LDGF' "LDG Front" pos:[112,210] width:80 height:25 align:#left border:true
		button 'PreSet_LDGR' "LDG Rear" pos:[112,240] width:80 height:25 align:#left border:true
		button 'PreSet_LDGRight' "LDG Right" pos:[195,210] width:80 height:25 align:#left border:true
		button 'PreSet_LDGRLeft' "LDG Left" pos:[195,240] width:80 height:25 align:#left border:true
		
		button 'PreSet_Turret' "Turret" pos:[195,210] width:80 height:25 align:#left border:true
		
		button 'MakeThruster' "Thruster" pos:[350,200] width:120 height:40 align:#left border:true 

 	
	on MakeSpline pressed do
	(
	--Create Controller	
	Rectangle length:X_Size.value width:Y_Size.value cornerRadius:0 pos:[0,0,0] name: (CRTL_Name.text+"_Controller") isSelected:on	
	modPanel.addModToSelection (EmptyModifier ()) ui:on		
	
	$.wirecolor = color 88 177 27

	----Add First Parameter	

if 	SyncBox.state==false do
	(	
	   AnimationSlider = attributes SliderData
   (
   parameters main rollout:params
   (
   AnimationSlider type: #float ui:AnSlider default:0

   )
   rollout params "Animation Parameters"
   ( 
	slider AnSlider Animation_Name.text type: #float range:[TimeScaleMin.value,TimeScaleMax.value,0]

    )
   )
	custAttributes.add $.modifiers[1] AnimationSlider
)   
		


		
		
	---Optional Mirror SpinnerControl

if 	SyncBox.state==true do
(	
	   AnimationSlider = attributes SliderData
   (
   parameters main rollout:params
   (
   AnimationSlider type: #float ui:AnSlider default:0
   AnimationSpinner type: #float ui:AnSpinner default:0
   )
   rollout params "Animation_Parameters"
   ( 
	slider AnSlider Animation_Name.text type: #float range:[TimeScaleMin.value,TimeScaleMax.value,0]
	spinner AnSpinner "M_Sp" type: #float range:[TimeScaleMin.value,TimeScaleMax.value,0]
    )
   )
	custAttributes.add $.modifiers[1] AnimationSlider
   

   
   
  --link AB 
   

	paramWire.connect2way $.modifiers[#Attribute_Holder].SliderData[#AnimationSlider] $.modifiers[#Attribute_Holder].SliderData[#AnimationSpinner] "AnimationSpinner" "AnimationSlider"
   
 
   
)  
		

		
		
	)
		
	-----thruster
		on MakeThruster pressed do
	(
	--Create Controller	
	Circle radius:50 cornerRadius:0 pos:[0,0,0] name: ("Thruster_Controller") isSelected:on	
	modPanel.addModToSelection (EmptyModifier ()) ui:on		
	
	$.wirecolor = color 176 26 26

	
	AnimationSlider = attributes SliderData
   (
   parameters main rollout:params
   (
   AnimationSlider type: #float ui:AnSlider default:0
   AnimationSpinner type: #float ui:AnSpinner default:0
   )
   rollout params "Thruster_Parameters"
   ( 
	slider AnSlider "Thruster" type: #float range:[-1,100,0]
	spinner AnSpinner "Thruster Exact" type: #float range:[-1,100,0]
    )
   )
	custAttributes.add $.modifiers[1] AnimationSlider
   

   
   

	paramWire.connect2way $.modifiers[#Attribute_Holder].SliderData[#AnimationSlider] $.modifiers[#Attribute_Holder].SliderData[#AnimationSpinner] "AnimationSpinner" "AnimationSlider"
   
 
   
)  
		

		
		
	
	
	
	
	
	
	
	
	-------------------PRESETS-------------------PRESETS-----------------	
	on PreSet_DOOR pressed do
	(
		CRTL_Name.text="Door"
		Animation_Name.text="Open"
		TimeScaleMax.value=30
		TimeScaleMin.value=0
		X_Size.value=50
		Y_Size.value=50
		SyncBox.state=false
	)
	
	on PreSet_RESET pressed do
	(
		CRTL_Name.text="Generic"
		Animation_Name.text="Animation"
		TimeScaleMax.value=30
		TimeScaleMin.value=0
		X_Size.value=50
		Y_Size.value=50
		SyncBox.state=false
	)
	--Landing Gear
		on PreSet_LDGF pressed do
	(
		CRTL_Name.text="LDG_Front"
		Animation_Name.text="LDG_Cond"
		TimeScaleMax.value=100
		TimeScaleMin.value=-100
		X_Size.value=150
		Y_Size.value=250
		SyncBox.state=true
	)
	
		on PreSet_LDGR pressed do
	(
		CRTL_Name.text="LDG_Rear"
		Animation_Name.text="LDG_Cond"
		TimeScaleMax.value=100
		TimeScaleMin.value=-100
		X_Size.value=150
		Y_Size.value=250
		SyncBox.state=true
	)
	
		on PreSet_LDGRight pressed do
	(
		CRTL_Name.text="LDG_Right"
		Animation_Name.text="LDG_Cond"
		TimeScaleMax.value=100
		TimeScaleMin.value=-100
		X_Size.value=150
		Y_Size.value=250
		SyncBox.state=true
	)
		
		on PreSet_LDGRLeft pressed do
	(
		CRTL_Name.text="LDG_Left"
		Animation_Name.text="LDG_Cond"
		TimeScaleMax.value=100
		TimeScaleMin.value=-100
		X_Size.value=150
		Y_Size.value=250
		SyncBox.state=true
	)
	
		on Turret pressed do
	(
		CRTL_Name.text="Turret"
		Animation_Name.text="Deploy"
		TimeScaleMax.value=150
		TimeScaleMin.value=0
		X_Size.value=150
		Y_Size.value=450
		SyncBox.state=false
	)	
	

	
	
		-------------------PRESETS-------------------PRESETS-----------------	
)

createDialog Anim_Controller_Maker
