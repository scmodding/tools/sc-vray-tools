try (closeRolloutFloater Stand_Light_Editors) catch()


rollout Stand_Light_Editors "Standard Light Overrider" width:300 height:920
(	
    label 'Titles' "Overrider only works with Standard Lights" pos:[38,10] width:303 height:20 align:#left
	
	spinner 'Intensity_amount' "Intensity" pos:[50,35] scale:0.01 width:70 height:30 align:#left range:[0,10000000,100]
	button 'Intensity_Set' "SET" pos:[160,35] width:70 height:15
	colorpicker 'theColor' "Color:" color:[255,255,255] modal:false pos:[70,55]
	button 'Color_Set' "SET" pos:[160,55] width:70 height:15	
	groupBox 'Decays' "Decay" pos:[30,130] width:230 height:100 align:#left
	dropdownlist 'DecayType' "Type:" items:#("None", "Inverse", "Inverse Square") pos: [50,150] width:100 height:4
	button 'DT_Set' "SET" pos:[160,170] width:70 height:15	
	spinner 'DT_Start' "Start:" pos:[50,195] scale:0.001 width:70 height:30 align:#left range:[0,10000000,1]
	button 'DT_Start_Set' "SET" pos:[160,195] width:70 height:15
	groupBox 'N_Atten' "Near Attenuation" pos:[30,235] width:230 height:85 align:#left
	spinner 'N_Atten_Start' "Start:" pos:[70,275] scale:0.001 width:70 height:30 align:#left range:[0,10000000,0]	
	spinner 'N_Atten_End' "End: " pos:[73,295] scale:0.001 width:70 height:30 align:#left range:[0,10000000,1]	
	button 'N_Atten_Start_SET' "SET" pos:[160,275] width:70 height:15
	button 'N_Atten_End_Set' "SET" pos:[160,295] width:70 height:15
	button 'N_Atten_ON' "ON" pos:[80,255] width:70 height:15
	button 'N_Atten_OFF' "OFF" pos:[150,255] width:70 height:15
	groupBox 'F_Atten' "Far Attenuation" pos:[30,325] width:230 height:85 align:#left
	spinner 'F_Atten_Start' "Start:" pos:[70,365] scale:0.001 width:70 height:30 align:#left range:[0,10000000,10]	
	spinner 'F_Atten_End' "End: " pos:[73,385] scale:0.001 width:70 height:30 align:#left range:[0,10000000,100]	
	button 'F_Atten_Start_SET' "SET" pos:[160,365] width:70 height:15
	button 'F_Atten_End_Set' "SET" pos:[160,385] width:70 height:15
	button 'F_Atten_ON' "ON" pos:[80,345] width:70 height:15
	button 'F_Atten_OFF' "OFF" pos:[150,345] width:70 height:15
	groupBox 'ADV_effects' "Advanced Effects:" pos:[30,410] width:230 height:120 align:#left
	spinner 'AE_Contrat' "Contrast: " pos:[50,430] scale:0.1 width:70 height:30 align:#left range:[0,100,100]		
	button 'AE_Contrat_SET' "SET" pos:[160,430] width:70 height:15
	spinner 'AE_SoftDiff' "Soften Diff: " pos:[40,450] scale:0.1 width:70 height:30 align:#left range:[0,100,100]		
	button 'AE_SoftDiff_SET' "SET" pos:[160,450] width:70 height:15	
	label 'AE_Diff' "Diffuse" pos:[90,470] width:40 height:15	
	button 'AE_Diff_ON' "ON" pos:[130,470] width:50 height:15
	button 'AE_Diff_OFF' "OFF" pos:[180,470] width:50 height:15
	label 'AE_Spec' "Specular" pos:[90,490] width:40 height:15	
	button 'AE_Spec_ON' "ON" pos:[130,490] width:50 height:15
	button 'AE_Spec_OFF' "OFF" pos:[180,490] width:50 height:15
	label 'AE_AB' "Ambient" pos:[90,510] width:40 height:15	
	button 'AE_AB_ON' "ON" pos:[130,510] width:50 height:15
	button 'AE_AB_OFF' "OFF" pos:[180,510] width:50 height:15
	groupBox 'SDP' "Shadow Parameters:" pos:[30,535] width:230 height:120 align:#left
	colorpicker 'ShadColor' "Shadow Color:" color:[0,0,0] modal:false pos:[40,555]
	button 'ShadColor_SET' "SET" pos:[180,556] width:50 height:15
	spinner 'SD_Den' "Density: " pos:[60,580] scale:0.001 width:70 height:30 align:#left range:[0,1.0,1.0]			
	button 'SD_Den_SET' "SET" pos:[180,580] width:50 height:15
	label 'SD_LightAffects' "Light Affects Shadow Color" pos:[80,605] width:200 height:15		
	button 'SD_LightAffects_ON' "ON" pos:[95,620] width:50 height:15
	button 'SD_LightAffects_OFF' "OFF" pos:[145,620] width:50 height:15
	groupBox 'AtmoShadow' "Atmosphere Shadows:" pos:[30,655] width:230 height:115 align:#left
	label 'AtmoShap_State' "ON" pos:[50,680] width:60 height:15		
	button 'AtmoShap_State_ON' "ON" pos:[95,680] width:50 height:15
	button 'AtmoShap_State_OFF' "OFF" pos:[145,680] width:50 height:15
	spinner 'AS_Opac' "Opacity: " pos:[60,705] scale:0.1 width:70 height:30 align:#left range:[0,100,100]		
	button 'AS_Opac_SET' "SET" pos:[180,705] width:50 height:15
	spinner 'AS_CA' "Color Amount: " pos:[40,730] scale:0.1 width:70 height:30 align:#left range:[0,100,100]		
	button 'AS_CA_SET' "SET" pos:[180,730] width:50 height:15
	groupBox 'SpotliteParameters' "Spotlight Parameters:" pos:[30,770] width:230 height:140 align:#left	
	label 'OvershootsA' "Overshoot" pos:[50,790] width:60 height:15	
	button 'Overshoot_ON' "ON" pos:[130,790] width:50 height:15
	button 'Overshoot_OFF' "OFF" pos:[180,790] width:50 height:15
	spinner 'HotBeam_Spot' "Hotspot:" pos:[65,820] scale:0.1 width:70 height:30 align:#left range:[0,179.5,10]
	spinner 'Fallotff_Spot' "Falloff:" pos:[70,840] scale:0.1 width:70 height:30 align:#left range:[0,179.5,90]
	button 'Hotbeam_Spot_set' "SET" pos:[180,820] width:50 height:15
	button 'Fallotff_Spot_set' "SET" pos:[180,840] width:50 height:15
	button 'Spot_circle' "Circle" pos:[75,860] width:70 height:15
	button 'Spot_rect' "Rectangle" pos:[145,860] width:70 height:15
	spinner 'Rect_Aspect' "Aspect:" pos:[65,885] scale:0.01 width:70 height:30 align:#left range:[0,100,1]	
	button 'Rect_Aspect_set' "SET" pos:[180,885] width:50 height:15	
	button 'DT_Set_fix' "x/10" pos:[110,212] width:70 height:15		
	groupBox 'Shadow' "Shadows:" pos:[30,70] width:230 height:55 align:#left
	button 'SH_ON' "ON" pos:[95,82] width:50 height:15
	button 'SH_OFF' "OFF" pos:[145,82] width:50 height:15
	dropdownlist 'ShadowType' "" items:#("Adv RT", "Area Shadows", "ShadowMap", "RT Shadow", "VRay", "XShadow") pos: [50,100] width:100 height:6
	button 'SH_SET_TYPE' "SET" pos:[165,105] width:50 height:15
---shadows
	on SH_ON pressed do
	(
		try 
		(
		$selection.castShadows   =True 
		)
		
		catch (messagebox "Not a Standard Light")
	)

	
		on SH_OFF castShadows do
	(
		try 
		(
		$selection.overShoot   =FALSE
		)
		
		catch (messagebox "Not a Standard Light")
	)
	
	on SH_SET_TYPE pressed do
	(
		try 
		(
		if ShadowType.items[ShadowType.selection] == "Adv RT" do $selection.shadowGenerator=Advanced_Ray_traced()
		
		if ShadowType.items[ShadowType.selection] == "Area Shadows" do $selection.shadowGenerator=Area_Shadows()

		if ShadowType.items[ShadowType.selection] == "ShadowMap" do $selection.shadowGenerator=ShadowMap()

		if ShadowType.items[ShadowType.selection] == "RT Shadow" do $selection.shadowGenerator=raytraceShadow()
		
		if ShadowType.items[ShadowType.selection] == "VRay" do $selection.shadowGenerator=VRayShadow()

		if ShadowType.items[ShadowType.selection] == "XShadow" do $selection.shadowGenerator=XShadow()				
		
		)
		
		catch (messagebox "Not a Standard Light")
	)	

--overshoot
	on Overshoot_ON pressed do
	(
		try 
		(
		$selection.overShoot   =True 
		)
		
		catch (messagebox "Not a Standard Light")
	)

	
		on Overshoot_OFF pressed do
	(
		try 
		(
		$selection.overShoot   =FALSE
		)
		
		catch (messagebox "Not a Standard Light")
	)
	--hotspot and falloff
		on Hotbeam_Spot_set pressed do
	(
		try 
		(
		$selection.hotspot =HotBeam_Spot.value
		)
		
		catch (messagebox "Not a Standard Light")
	)
	
	
		on Fallotff_Spot_set pressed do
	(
		try 
		(
		$selection.falloff  =Fallotff_Spot.value
		)
		
		catch (messagebox "Not a Standard Light")
	)
	
--spotshape
	on Spot_circle pressed do
	(
		try 
		(
		$selection.coneShape =1 
		)
		
		catch (messagebox "Not a Standard Light")
	)

	
		on Spot_rect pressed do
	(
		try 
		(
		$selection.coneShape =2
		)
		
		catch (messagebox "Not a Standard Light")
	)
	
		on Rect_Aspect_set pressed do
	(
		try 
		(
		$selection.aspect =Rect_Aspect.value
		)
		
		catch (messagebox "Not a Standard Light")
	)

--multiplier
	on Intensity_Set pressed do
	(
		try 
		(
		$selection.multiplier=Intensity_amount.value
		)
		
		catch (messagebox "Not a Standard Light")
	)
--color
	on Color_Set pressed do
	(
		try 
		(
		$selection.rgb=theColor.color
		)
		
		catch (messagebox "Not a Standard Light")
	)
--decays
	on DT_Set pressed do
	(
		try 
		(
		if DecayType.items[DecayType.selection] == "None" do $selection.attenDecay=1
		
		if DecayType.items[DecayType.selection] == "Inverse" do $selection.attenDecay=2

		if DecayType.items[DecayType.selection] == "Inverse Square" do $selection.attenDecay=3		
		
		)
		
		catch (messagebox "Not a Standard Light")
	)	
	
	on DT_Start_Set pressed do
	(
		try 
		(
		$selection.DecayRadius=DT_Start.value*100
		)
		
		catch (messagebox "Not a Standard Light")
	)

		on DT_Set_fix pressed do
	(
		try 
		(
		$selection.DecayRadius=$selection.DecayRadius*.01
		)
		
		catch (messagebox "Not a Standard Light")
	)	
		
	
--Near_Atten
	on N_Atten_Start_SET pressed do
	(
		try 
		(
		$selection.nearAttenStart=N_Atten_Start.value*100
		)
		
		catch (messagebox "Not a Standard Light")
	)	
	
	on N_Atten_End_Set pressed do
	(
		try 
		(
		$selection.nearAttenEnd=N_Atten_End.value*100
		)
		
		catch (messagebox "Not a Standard Light")
	)	

	on N_Atten_ON pressed do
	(
		try 
		(
		$selection.useNearAtten=TRUE
		)
		
		catch (messagebox "Not a Standard Light")
	)	

	on N_Atten_OFF pressed do
	(
		try 
		(
		$selection.useNearAtten=FALSE
		)
		
		catch (messagebox "Not a Standard Light")
	)

--Far_Atten	
	on F_Atten_Start_SET pressed do
	(
		try 
		(
		$selection.FarAttenStart=F_Atten_Start.value*100
		)
		
		catch (messagebox "Not a Standard Light")
	)	
	
	on F_Atten_End_Set pressed do
	(
		try 
		(
		$selection.FarAttenEnd=F_Atten_End.value*100
		)
		
		catch (messagebox "Not a Standard Light")
	)	

	on F_Atten_ON pressed do
	(
		try 
		(
		$selection.useFarAtten=TRUE
		)
		
		catch (messagebox "Not a Standard Light")
	)	

	on F_Atten_OFF pressed do
	(
		try 
		(
		$selection.useFarAtten=FALSE
		)
		
		catch (messagebox "Not a Standard Light")
	)
--Ad_EFF
	on AE_Contrat_SET pressed do
	(
		try 
		(
		$selection.contrast=AE_Contrat.value
		)
		
		catch (messagebox "Not a Standard Light")
	)
	
	on AE_SoftDiff_SET pressed do
	(
		try 
		(
		$selection.softenDiffuseEdge =AE_SoftDiff.value
		)
		
		catch (messagebox "Not a Standard Light")
	)
--diffuse 
	on AE_Diff_ON pressed do
	(
		try 
		(
		$selection.affectDiffuse =TRUE
		)
		
		catch (messagebox "Not a Standard Light")
	)	

	on AE_Diff_OFF pressed do
	(
		try 
		(
		$selection.affectDiffuse =FALSE
		)
		
		catch (messagebox "Not a Standard Light")
	)
	
	
	on AE_Spec_ON pressed do
	(
		try 
		(
		$selection.affectSpecular  =TRUE
		)
		
		catch (messagebox "Not a Standard Light")
	)	

	on AE_Spec_OFF pressed do
	(
		try 
		(
		$selection.affectSpecular  =FALSE
		)
		
		catch (messagebox "Not a Standard Light")
	)
	
	on AE_AB_ON pressed do
	(
		try 
		(
		$selection.ambientOnly  =TRUE
		)
		
		catch (messagebox "Not a Standard Light")
	)	

	on AE_AB_OFF pressed do
	(
		try 
		(
		$selection.ambientOnly  =FALSE
		)
		
		catch (messagebox "Not a Standard Light")
	)
	
	--shadow Parameters
		on ShadColor_SET pressed do
	(
		try 
		(
		$selection.ShadowColor =ShadColor.color
		)
		
		catch (messagebox "Not a Standard Light")
	)

	on SD_Den_SET pressed do
	(
		try 
		(
		$selection.shadowMultiplier=SD_Den.value
		)
		
		catch (messagebox "Not a Standard Light")
	)

--AtmosphereShadsows
		on AtmoShap_State_ON pressed do
	(
		try 
		(
		$selection.atmosShadows   =TRUE
		)
		
		catch (messagebox "Not a Standard Light")
	)	

	on AtmoShap_State_OFF pressed do
	(
		try 
		(
		$selection.atmosShadows   =FALSE
		)
		
		catch (messagebox "Not a Standard Light")
	)

	on AS_Opac_SET pressed do
	(
		try 
		(
		$selection.atmosOpacity =AS_Opac.value
		)
		
		catch (messagebox "Not a Standard Light")
	)

	on AS_CA_SET pressed do
	(
		try 
		(
		$selection.atmosColorAmt  =AS_CA.value
		)
		
		catch (messagebox "Not a Standard Light")
	)

)

createDialog Stand_Light_Editors
