	rollout SkelliAlignTool "Align Tool" width:330 height:300
	(
		
		button 'Align' "Align (POS, ROT)" pos:[50,60] width:220 height:30 align:#left border:true
		label 'instructions' "Selected Objects will align with unselected objects" pos:[40,100]
		label 'instructions2' "Adjust Pivot Rotation by 90 on the X axis via offset:world" pos:[40,15]
		label 'instructions3' "then by 90 on the z axis via offset:world" pos:[70,31]
		
		button 'AlignPOS' "Align (POS)" pos:[50,130] width:220 height:30 align:#left border:true
		--specific parameters
		button 'AlignROT' "Align (ROT)" pos:[50,200] width:220 height:30 align:#left border:true
		
		button 'AlignPOS_X' "X" pos:[100,160] width:30 height:30 align:#left border:true
		button 'AlignPOS_Y' "Y" pos:[140,160] width:30 height:30 align:#left border:true
		button 'AlignPOS_Z' "Z" pos:[180,160] width:30 height:30 align:#left border:true
		
		button 'AlignR_X' "X" pos:[100,230] width:30 height:30 align:#left border:true
		button 'AlignR_Y' "Y" pos:[140,230] width:30 height:30 align:#left border:true
		button 'AlignR_Z' "Z" pos:[180,230] width:30 height:30 align:#left border:true
		
		on Align pressed do
		(
		for sel in selection do (
	    unselected = for obj in objects where not obj.isSelected and obj.name == sel.name collect obj
	    if unselected.count > 0 then (
	        match = unselected[1]
			in coordsys world sel.rotation = match.rotation
			in coordsys world sel.pos = match.pos 

	    )
		)	
		)



		on AlignPOS pressed do
		(
		for sel in selection do (
	    unselected = for obj in objects where not obj.isSelected and obj.name == sel.name collect obj
	    if unselected.count > 0 then (
	        match = unselected[1]
			in coordsys world sel.pos = match.pos 

	    )
		)	
		)		
		
		---position only rotation
		on AlignROT pressed do
		(
		for sel in selection do (
	    unselected = for obj in objects where not obj.isSelected and obj.name == sel.name collect obj
	    if unselected.count > 0 then (
	        match = unselected[1]
			in coordsys world sel.rotation = match.rotation

	    )
		)	
		)
		
		---align individual postitions
		---x
		on AlignPOS_X pressed do
		(
		for sel in selection do (
	    unselected = for obj in objects where not obj.isSelected and obj.name == sel.name collect obj
	    if unselected.count > 0 then (
	        match = unselected[1]
			in coordsys world sel.pos.x = match.pos.x

	    )
		)	
		)	
		
		---Y Position
		on AlignPOS_Y pressed do
		(
		for sel in selection do (
	    unselected = for obj in objects where not obj.isSelected and obj.name == sel.name collect obj
	    if unselected.count > 0 then (
	        match = unselected[1]
			in coordsys world sel.pos.y = match.pos.y

	    )
		)	
		)		
		
		---Z
		on AlignPOS_Z pressed do
		(
		for sel in selection do (
	    unselected = for obj in objects where not obj.isSelected and obj.name == sel.name collect obj
	    if unselected.count > 0 then (
	        match = unselected[1]
			in coordsys world sel.pos.z = match.pos.z

	    )
		)	
		)	
		
		----rotation indvid
		
		on AlignR_X pressed do
		(
		for sel in selection do (
	    unselected = for obj in objects where not obj.isSelected and obj.name == sel.name collect obj
	    if unselected.count > 0 then (
	        match = unselected[1]
			in coordsys world sel.rotation.x = match.rotation.x

	    )
		)	
		)	
		
		---Y Position
		on AlignR_Y pressed do
		(
		for sel in selection do (
	    unselected = for obj in objects where not obj.isSelected and obj.name == sel.name collect obj
	    if unselected.count > 0 then (
	        match = unselected[1]
			in coordsys world sel.rotation.y = match.rotation.y

	    )
		)	
		)		
		
		---Z
		on AlignR_Z pressed do
		(
		for sel in selection do (
	    unselected = for obj in objects where not obj.isSelected and obj.name == sel.name collect obj
	    if unselected.count > 0 then (
	        match = unselected[1]
			in coordsys world sel.rotation.z = match.rotation.z

	    )
		)	
		)		
		
		
		
		
		
		----

	)

	createDialog SkelliAlignTool
	
--Code was paritially developed with BingAI