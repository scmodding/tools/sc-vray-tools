try (closeRolloutFloater QuadMaster) catch()

	rollout QuadMaster "QuadMaster" width:300 height:500
	(

	
		button 'ModelPrep' "Model Prep" pos:[82,364] width:88 height:33 toolTip:"" align:#left
		label 'lbl1' "QUADMASTER" pos:[115,20] width:145 height:44 align:#left
		button 'QuadScene' "Quad Scene" pos:[65,400] width:180 height:63 toolTip:"" align:#left
		groupBox 'grp_auto' "Automated Functions" pos:[13,345] width:275 height:135 align:#left
		groupBox 'grp_quad' "Quadrify" pos:[13,260] width:275 height:80 align:#left
		button 'Quadrify_Btn' "Quadrify Selection" pos:[80,290] width:150 height:33 toolTip:"" align:#left
		
		groupBox 'grp_select' "Selection" pos:[13,60] width:275 height:180 align:#left
		button 'SelectMesh7' "Select Mesh" pos:[80,80] width:150 height:33 toolTip:"" align:#left			
		button 'US_Skin' "Deselect SKIN" pos:[80,120] width:150 height:33 toolTip:"" align:#left
		button 'BaseMesh' "Select Basic Mesh" pos:[80,160] width:150 height:33 toolTip:"" align:#left
		button 'QuadScene2' "X2" pos:[245,400] width:30 height:63 toolTip:"" align:#left
		button 'ModelPrepS' "Model Prep Scene" pos:[170,364] width:110 height:33 toolTip:"" align:#left
		button 'Skinprep' "SkinCycle" pos:[20,364] width:63 height:33 toolTip:"" align:#left	
		--selection
		on SelectMesh7 pressed do
			(
	--ChatGPT dervived
	-- Clear any previous selection
	clearSelection()

	-- Iterate through all objects in the scene
	for obj in geometry do
	(
	    -- Check if the object is not a VRayVolumeGrid and has an editable mesh or editable poly modifier applied
	    if (not isKindOf obj VRayVolumeGrid) and (classOf obj.baseObject == Editable_Mesh or classOf obj.baseObject == Editable_Poly) do
	    (
	        -- Add the object to the selection
	        selectMore obj
	    )
	)

	-- If there are objects selected, print a message
	if selection.count > 0 then
	(
	    print ("Selected " + selection.count as string + " geometry objects.")
	)
	else
	(
	    print "No geometry objects with editable mesh or editable poly found."
	)
			)	

		on US_Skin pressed do
		(
		
				fn hasSkinModifier obj =
				(

					for mod in obj.modifiers where classof mod == Skin do
					(

						return true
					)

					false
				)

				sel = getCurrentSelection()


				for obj in sel where hasSkinModifier obj do
				(

					deselect obj
				)
	
		)
	--Quad
		on Quadrify_Btn pressed do
			(
				convertTo $ PolyMeshObject	
				arr = for s in selection collect s
				for obj in arr do (select obj; PolyToolsModeling.Quadrify false false)
				macros.run "Modifier Stack" "Convert_to_Mesh"
			)
		
--Automated
		
		on ModelPrep pressed do
		(
			--weighted Normal

	modPanel.addModToSelection (Weighted_Normals ()) ui:on
	---Magenta on Standard Colors		
				whiteClr = (color 255 12 255 )
			for o in selection where (mat = o.material) != undedined do
			(		
				case classOf mat of
				(
					standardMaterial: mat.diffuse = whiteClr

					Multimaterial:
					(
						for i = 1 to mat.numsubs do
						(
							case classOf mat[i] of
							(
								standardMaterial: mat[i].diffuse = whiteClr
							)
						)
					)
				)
			)
	--- Collapse 

				
	--- convert to editable poly
	convertTo $ PolyMeshObject		
			
	---Quadify_Mesh
			
		arr = for s in selection collect s
		for obj in arr do (select obj; PolyToolsModeling.Quadrify false false)		
			
	---Converter to editable mesh
	macros.run "Modifier Stack" "Convert_to_Mesh"	
			
		)




		
		on QuadScene pressed do
		(
			SelectMesh7.pressed ()
			US_Skin.pressed ()
			Quadrify_Btn.pressed ()
			SelectMesh7.pressed ()
			US_Skin.pressed ()
		)
		
		on BaseMesh presssed do
		(
			SelectMesh7.pressed ()
			US_Skin.pressed ()		
		)
		on QuadScene2 pressed do
		(
			QuadScene.pressed ()
			QuadScene.pressed ()
			messageBox "Scene Quadrified"			
		)

			on ModelPrepS presssed do
			(
				SelectMesh7.pressed ()
				US_Skin.pressed ()	
				ModelPrep.pressed ()
				SelectMesh7.pressed ()
				US_Skin.pressed ()					
			)
on Skinprep pressed do
(
for obj in objects do (
    if classOf obj == Editable_Mesh do (
        for mod in obj.modifiers do (
            if classOf mod == Skin do (
                mod.always_deform = false
                mod.always_deform = true
            )
        )
    )
)
)

		
	)
	CreateDialog QuadMaster