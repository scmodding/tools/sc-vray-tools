try (closeRolloutFloater SC_SHIPLayerUpdater) catch()

rollout SC_SHIPLayerUpdater "Ship Layer Updater" width:500 height:200
(


	button 'CreateLayer1' "Missing Headlights" pos:[130,109] width:120 height:33 toolTip:"Creates new layers to help organize ship models" align:#left
	button 'CreateLayer2' "Missing Root" pos:[250,109] width:120 height:33 toolTip:"Creates new layers to help organize ship models" align:#left
	button 'CreateLayer3' "Missing Both" pos:[200,150] width:120 height:33 toolTip:"Creates new layers to help organize ship models" align:#left	
	editText 'ShipName' "Ship Name" pos:[37,76] width:385 height:20 align:#left
	
	on CreateLayer1 pressed do
	(
		
		LayerManager.newLayerFromName (Shipname.text + "_LIGHTS_Headlights")
		LayerManager.newLayerFromName (Shipname.text + "_LIGHTS_Headlights_AUX")
		LayerManager.newLayerFromName (Shipname.text + "_LIGHTS_Headlights_EMG")
		LayerManager.newLayerFromName (Shipname.text + "_LIGHTS_Headlights_STD")		
	)
	
	on CreateLayer2 pressed do
	(
		
		LayerManager.newLayerFromName (Shipname.text + "_Model")
		LayerManager.newLayerFromName (Shipname.text + "_Root")				
	)
	
	on CreateLayer3 pressed do
	(
		CreateLayer1.pressed()
		CreateLayer2.pressed()
		
	)
)

CreateDialog SC_SHIPLayerUpdater
