(

			for o in selection where (mat = o.material) != undedined do
			(		
				case classOf mat of
				(
					VrayMtl: 
					(
						case classOf mat.texmap_opacity of
						(MultiOutputChannelTexmapToTexmap:	
						(
						mat.texmap_opacity = copy mat.texmap_opacity
						mat.texmap_opacity.name="POM Alpha"
						))
					)
					VRayBlendMtl: 
					(
						case classOf mat.baseMtl of
						(VRayMtl:	
							(case classOf mat.baseMtl.texmap_opacity of
								(
							MultiOutputChannelTexmapToTexmap:
									(
									mat.baseMtl.texmap_opacity = copy mat.baseMtl.texmap_opacity
									mat.baseMtl.texmap_opacity.name="POM Alpha Base"
									mat.coatMtl[1].texmap_opacity = copy mat.coatMtl[1].texmap_opacity
									mat.coatMtl[1].texmap_opacity.name="POM Alpha Coat 1"
									)
								)
							)
						)
					)
							

					Multimaterial:
					(
						for i = 1 to mat.numsubs do
						(
							case classOf mat[i] of
							(
								VrayMtl: 
								(
								case classOf mat[i].texmap_opacity of
								(MultiOutputChannelTexmapToTexmap:
								(
								mat[i].texmap_opacity = copy mat[i].texmap_opacity
								mat[i].texmap_opacity.name="POM Alpha"
								))
								)
								
								
								
								
								
								
								VRayBlendMtl:
								( 
									case classOf mat[i].baseMtl of
									(
										VRayMtl:
											(		
												case classOf mat[i].baseMtl.texmap_opacity of
												(MultiOutputChannelTexmapToTexmap:	
												(
												mat[i].baseMtl.texmap_opacity = copy mat[i].baseMtl.texmap_opacity
												mat[i].baseMtl.texmap_opacity.name="POM Alpha Base"
												
												mat[i].coatMtl[1].texmap_opacity = copy mat[i].coatMtl[1].texmap_opacity
												mat[i].coatMtl[1].texmap_opacity.name="POM Alpha Coat 1"
												)
												)
											)
									)
								)







								
							)
						)
				)
				)
			)
			
		)