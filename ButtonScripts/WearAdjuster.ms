try (closeRolloutFloater Wear_Adjuster) catch()



rollout Wear_Adjuster "Wear Adjuster 0.0.1" width:309 height:190
(
	label 'lbl1' "Wear Adjuster" pos:[113,17] width:80 height:27 align:#left
	spinner 'spn1' "Mask Output" pos:[75,58] width:83 height:16 align:#left
	button 'Set_Blend' "Blend Material (Factor)" pos:[22,93] width:133 height:38 align:#left
	button 'set_WDA' "WDA Material" pos:[164,93] width:133 height:38 align:#left
	button 'Set_Cloth' "Cloth Material" pos:[163,136] width:133 height:38 align:#left
	button 'Set_wear' "Blend Material (Wear)" pos:[24,137] width:133 height:38 align:#left
	
	on Set_Blend pressed do
	(
		meditMaterials[activeMeditSlot].texmap_blend[1].output.output_amount=spn1.value
	)
	
	on Set_wear pressed do
	(
		meditMaterials[activeMeditSlot].Blend[1] = color (spn1.value*255) (spn1.value*255) (spn1.value*255)
	)
	
	on set_WDA pressed do
	(
		meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].color=color (spn1.value*255) (spn1.value*255) (spn1.value*255) 255
	)
	
	on Set_Cloth pressed do
	(
		meditMaterials[activeMeditSlot].basemtl.texmap_blend[1].maplist[2].color=color (spn1.value*255) (spn1.value*255) (spn1.value*255) 255
		meditMaterials[activeMeditSlot].coatmtl[1].texmap_blend[1].maplist[2].color=color (spn1.value*255) (spn1.value*255) (spn1.value*255) 255
		meditMaterials[activeMeditSlot].coatmtl[1].texmap_blend[1].maplist[2].color=color (spn1.value*255) (spn1.value*255) (spn1.value*255) 255
		meditMaterials[activeMeditSlot].coatmtl[2].texmap_blend[1].maplist[2].color=color (spn1.value*255) (spn1.value*255) (spn1.value*255) 255
		meditMaterials[activeMeditSlot].coatmtl[3].texmap_blend[1].maplist[2].color=color (spn1.value*255) (spn1.value*255) (spn1.value*255) 255
	)
	
)


createDialog Wear_Adjuster