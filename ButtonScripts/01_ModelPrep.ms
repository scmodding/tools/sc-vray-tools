---3ds Max SC Model Prep--

--weighted Normal

modPanel.addModToSelection (Weighted_Normals ()) ui:on
---Magenta on Standard Colors		
			whiteClr = (color 255 12 255 )
		for o in selection where (mat = o.material) != undedined do
		(		
			case classOf mat of
			(
				standardMaterial: mat.diffuse = whiteClr

				Multimaterial:
				(
					for i = 1 to mat.numsubs do
					(
						case classOf mat[i] of
						(
							standardMaterial: mat[i].diffuse = whiteClr
						)
					)
				)
			)
		)
--- Collapse 

			
--- convert to editable poly
convertTo $ PolyMeshObject		
		
---Quadify_Mesh
		
	arr = for s in selection collect s
	for obj in arr do (select obj; PolyToolsModeling.Quadrify false false)		
		
---Converter to editable mesh
macros.run "Modifier Stack" "Convert_to_Mesh"