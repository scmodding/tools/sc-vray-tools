try

(
$.name=$.name + "_Controller"	
modPanel.addModToSelection (EmptyModifier ()) ui:on	
macros.run "Customize User Interface" "Custom_Attributes"
macros.run "Parameter Wire" "paramWire_dialog"
macros.run "Animation Tools" "OpenReactionManager"	

rollout SkelliAlignTool "Align Tool" width:330 height:150
	(
		
		button 'Align' "Align" pos:[50,60] width:220 height:30 align:#left border:true
			label 'instructions' "Selected Objects will align with unselected objects" pos:[40,100]
		label 'instructions2' "Adjust Pivot Rotation by 90 on the X axis via offset:world" pos:[40,15]
			label 'instructions3' "then by 90 on the z axis via offset:world" pos:[70,31]
		
		
		on Align pressed do
		(
		for sel in selection do (
	    unselected = for obj in objects where not obj.isSelected and obj.name == sel.name collect obj
	    if unselected.count > 0 then (
	        match = unselected[1]
			in coordsys world sel.rotation = match.rotation
			in coordsys world sel.pos = match.pos 

	    )
		)	
		)		
		


	)

createDialog SkelliAlignTool	
)
catch()