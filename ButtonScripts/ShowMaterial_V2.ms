for mat in scenematerials do
(
    -- Check if the material is a VrayBlendMtl
    if (classof mat == VrayBlendMtl) do
    (
        -- Access the base material slot
        
		(
		local baseMaterial = mat.basemtl

        -- Check if the base material name is "PREVIEW"
        if (baseMaterial isKindOf VRayMTL) and (findString mat.texmap_blend[1].maplist[2].name == "Wear_CONTROL" != undefined) do
        (
				try showTextureMap mat.basemtl on catch ()
        )
		)
		
		
		(
		local Coat1Material = mat.coatmtl[1]
		
		if (Coat1Material isKindOf VRayMTL) and (findString mat.texmap_blend[1].maplist[2].name == "Blend_CONTROL" != undefined) do
        (
				try showTextureMap mat.coatmtl[1] on catch ()
        )
		
		)
		
		
		(
		local ClothMtl = mat.coatmtl[4]
		
		if (ClothMtl isKindOf VRayMTL) and (mat.coatmtl[4].name == "PREIVEW") do
        (
				try showTextureMap mat.coatmtl[4] on catch ()
        )
		)
		
		
		
    )
	
	-- Check if the material is a VRayMtl and its name is "PREVIEW"
    if (classof mat == VRayMtl) do
    (
		
		try showTextureMap mat on catch ()
    )
	
	
	
	
	
)