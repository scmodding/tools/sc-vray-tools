-- Function to lowercase object names
fn lowercaseObjectNames =
(
    -- Get the selected objects
    local selectedObjects = getCurrentSelection()

    -- Iterate through each selected object
    for obj in selectedObjects do
    (
        -- Check if the object has a valid name
        if obj != undefined and obj.name != "" then
        (
            -- Lowercase the object's name
            obj.name = toLower obj.name
        )
    )
)

-- Call the function to lowercase object names
lowercaseObjectNames()