	if (classof obj == Editable_Mesh) then
	(
	convertTo $ PolyMeshObject	
	arr = for s in selection collect s
	for obj in arr do (select obj; PolyToolsModeling.Quadrify false false)
	macros.run "Modifier Stack" "Convert_to_Mesh"
	)
	
	if (classof obj == Editable_Poly) then
	(
	arr = for s in selection collect s
	for obj in arr do (select obj; PolyToolsModeling.Quadrify false false)
	macros.run "Modifier Stack" "Convert_to_Mesh"
	)