try (closeRolloutFloater WDAEquipSetGloss) catch()

rollout WDAEquipSetGloss "WDA Base Gloss Set" width:250 height:201
(
	label 'Base' "Base Gloss" pos:[50,20] width:100 height:18
	button 'Set_50' "Set Gloss 50" pos:[30,50] width:100 height:28 align:#left enabled:true
	button 'Set_100' "Set Gloss 100" pos:[30,100] width:100 height:28 align:#left enabled:true
	
	label 'Multiplier' "Multiplier Gloss" pos:[160,20] width:100 height:18
	button 'Set_100_M' "Set Gloss 100" pos:[140,70] width:90 height:38 align:#left enabled:true
	
	button 'ID_Mat' "ID Mat" pos:[10,150] width:50 height:20 align:#left enabled:true
	editText 'UpperGloss' "Base Gloss" pos:[84,150] width:102 height:16 range:[0,1,1] scale:1e-18 align:#left enabled:true
	editText 'LowerGloss' "Multi Gloss" pos:[84,170] width:102 height:16 range:[0,1,1] scale:1e-18 align:#left enabled:true	
	
	on Set_50 pressed do
	(
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].opacity[1]=50
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].opacity[2]=50
		
		UpperGloss.text=(meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].opacity[1]) as string
		LowerGloss.text=(meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.opacity[1]) as string
	)
	
	on Set_100 pressed do
	(
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].opacity[1]=100
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].opacity[2]=100
		
		UpperGloss.text=(meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].opacity[1]) as string
		LowerGloss.text=(meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.opacity[1]) as string
	)
	
	on Set_100_M pressed do
	(
		meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.opacity[1]=100
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.opacity[2]=100
		
		UpperGloss.text=(meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].opacity[1]) as string
		LowerGloss.text=(meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.opacity[1]) as string
	)
	
	on ID_Mat pressed do
	(
		UpperGloss.text=(meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].opacity[1]) as string
		LowerGloss.text=(meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.opacity[1]) as string
		
	)
)

	

createDialog WDAEquipSetGloss 
