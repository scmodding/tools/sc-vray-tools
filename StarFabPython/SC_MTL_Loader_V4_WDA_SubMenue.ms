
rollout WDASub "WDA Selection" width:150 height:90
(	
	button Load_WDAE "WDA BW [A]"  tooltip:"Used to older handle WDA material. Used for Star Citizen Build 3.xx and older "
	button Load_WDAEV2 "WDA V2 [A]" tooltip:"Use for current WDA material. Used for Star Citizen Build 3.xx and newer "
	button Load_WDAC "WDA Clothing [A]"  tooltip:"Used to older handle WDA material related to Clothing, Armor, and some Random Buildset items. Used for Star Citizen Build 3.xx and older "
	
		on Load_WDAE pressed do
	(
		fileIn("StarFabPython/SC_WDA_Equipment_Importer.ms")
	)

	
	on Load_WDAEV2 pressed do
	(
		filein ("StarFabPython/SC_WDA_Equipment_Importer_V2_WEAR.ms")
	)
	
	on Load_WDAC pressed do
	(
		filein ("StarFabPython/SC_WDA_Clothing_Importer.ms")
	)
	
)

	
CreateDialog WDASub