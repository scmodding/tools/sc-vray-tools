try (closeRolloutFloater POM_ALPHAFIX) catch()


rollout POM_ALPHAFIX "POM Alpha Fixer (Legacy Material)" width:300 height:150
(
	button 'Fix_Single' "Single Fix" pos:[60,20] width:160 height:30 align:#left border:true
	button 'Fix_Dual' "Dual Fix" pos:[60,60] width:160 height:30 align:#left border:true
	
	on Fix_Single pressed do
	(
		try
		(
		meditMaterials[activeMeditSlot].texmap_opacity = copy meditMaterials[activeMeditSlot].texmap_opacity
		meditMaterials[activeMeditSlot].texmap_opacity.name = "Alpha Map"
		)
		catch()
	)

	
	
	on Fix_Dual pressed do
	(
		try
		(
		meditMaterials[activeMeditSlot].baseMtl.texmap_opacity = copy meditMaterials[activeMeditSlot].baseMtl.texmap_opacity
		meditMaterials[activeMeditSlot].baseMtl.texmap_opacity.name = "Alpha Map"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_opacity = copy meditMaterials[activeMeditSlot].coatMtl[1].texmap_opacity
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_opacity.name = "Alpha Map"
		)
		catch()
	)
)

createDialog POM_ALPHAFIX 