try (closeRolloutFloater SC_Mass_Suf) catch()


rollout SC_Mass_Suf "SuffixRenamer" width:268 height:200
(
	
	button 'Suffix_EXT' "EXT" pos:[55,20] width:160 height:30 align:#left
	button 'Suffix_INT' "INT" pos:[55,55] width:160 height:30 align:#left
	button 'Suffix_BAYS' "BAYS" pos:[55,90] width:160 height:30 align:#left	
	button 'Suffix_CARGO' "CARGO" pos:[55,125] width:160 height:30 align:#left	
	
	-------Exterior------	
	
		on Suffix_EXT pressed do
		(
				for m in getClassInstances ColorCorrection do (
					if m.name == "Wear_CONTROL" do m.name = "Wear_CONTROL_EXT"
					if m.name == "DIRT_CONTROL" do m.name = "DIRT_CONTROL_EXT"
					if m.name == "Blend_CONTROL" do m.name = "Blend_CONTROL_EXT"
				)

				for m in getClassInstances CompositeTextureMap do (
					if m.name == "EDGE_DIRT_MASK" do m.name = "EDGE_DIRT_MASK_EXT"
					if m.name == "DIRT_INNER_MASK" do m.name = "DIRT_INNER_MASK_EXT"
				)

		)
	-------Interior------
	
		on Suffix_INT pressed do
		(
				for m in getClassInstances ColorCorrection do (
					if m.name == "Wear_CONTROL" do m.name = "Wear_CONTROL_INT"
					if m.name == "DIRT_CONTROL" do m.name = "DIRT_CONTROL_INT"
					if m.name == "Blend_CONTROL" do m.name = "Blend_CONTROL_INT"
				)

				for m in getClassInstances CompositeTextureMap do (
					if m.name == "EDGE_DIRT_MASK" do m.name = "EDGE_DIRT_MASK_INT"
					if m.name == "DIRT_INNER_MASK" do m.name = "DIRT_INNER_MASK_INT"
				)

		)	
	
	
-------	
	
		on Suffix_BAYS pressed do
		(
				for m in getClassInstances ColorCorrection do (
					if m.name == "Wear_CONTROL" do m.name = "Wear_CONTROL_BAYS"
					if m.name == "DIRT_CONTROL" do m.name = "DIRT_CONTROL_BAYS"
					if m.name == "Blend_CONTROL" do m.name = "Blend_CONTROL_BAYS"
				)

				for m in getClassInstances CompositeTextureMap do (
					if m.name == "EDGE_DIRT_MASK" do m.name = "EDGE_DIRT_MASK_BAYS"
					if m.name == "DIRT_INNER_MASK" do m.name = "DIRT_INNER_MASK_BAYS"
				)

		)		
	
	
		on Suffix_CARGO pressed do
		(
				for m in getClassInstances ColorCorrection do (
					if m.name == "Wear_CONTROL" do m.name = "Wear_CONTROL_CARGO"
					if m.name == "DIRT_CONTROL" do m.name = "DIRT_CONTROL_CARGO"
					if m.name == "Blend_CONTROL" do m.name = "Blend_CONTROL_CARGO"
				)

				for m in getClassInstances CompositeTextureMap do (
					if m.name == "EDGE_DIRT_MASK" do m.name = "EDGE_DIRT_MASK_CARGO"
					if m.name == "DIRT_INNER_MASK" do m.name = "DIRT_INNER_MASK_CARGO"
				)

		)		
	
	
	
	
	
)


CreateDialog SC_Mass_Suf