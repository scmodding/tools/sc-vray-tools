''''
Py tamplet by ventorvar
Completed by Baconator650

To Use:Load lights into a sperate .max scene and save, then load a your ship/OC model and replace(merge) the lights imported by this script into your ship/OC scene
Design to work with Standard Lights for better compatiblity with multiple render [may have to enable legacy modes on certain renders]
'''
import json
import pymxs #MaxScript Wrapper (disable if not using within 3ds Max) 
from pymxs import runtime as rt #MaxScript Wrapper (disable if not using within 3ds Max) 

SCBPLoad=rt.getOpenFileName(caption="Get that SCBP:",types="Star Citizen BluePrints (*.scbp)|*.scbp|""All those other Files|*.*")

PowerModifier=300 #Set to 30,0 for Vray Render; 1.0 for scanline, mental ray, or Cry Engine (1;1)
ScaleModifier=100 #Adjust for unit scale. 
KLightLoad=open(SCBPLoad)
bp = json.load(KLightLoad)

for soc_name, soc in bp['socs'].items():
    for lg_name, light_group in soc['lights'].items():
        for light_name, light in light_group.items():
            light_radius = float(light['EntityComponentLight']['sizeParams'].get('@lightRadius', 1.0))*ScaleModifier
            intensity = float(light['EntityComponentLight']['emergencyState'].get('@intensity', 1.0))*PowerModifier
            bulb_radius = float(light['EntityComponentLight']['sizeParams'].get('@bulbRadius', .01))*ScaleModifier
            #color
            Color_R = float(light['EntityComponentLight']['emergencyState']['color']['@r'])
            Color_G = float(light['EntityComponentLight']['emergencyState']['color']['@g'])
            Color_B = float(light['EntityComponentLight']['emergencyState']['color']['@b'])
            #projector
            FOV= float(light['EntityComponentLight']['projectorParams'].get('@FOV', 179.5))
            Focal_Beam= float(light['EntityComponentLight']['projectorParams'].get('@focusedBeam', 1.0))
            #light texture
            Light_Tex_Path=light['EntityComponentLight']['projectorParams']['@texture']
            texfiletypez='.tif'
            TextDDS_TIF=Light_Tex_Path.replace('.dds',texfiletypez)
            

            try:
                MinDistance=float(light['EntityComponentLight']['fadeParams'].get('@minDistance', 1.0))
            except KeyError:
                MinDistance=0.0

            try:
                maxDistance=float(light['EntityComponentLight']['fadeParams'].get('@maxDistance', 1.0))
            except KeyError:
                maxDistance=10
            presettag=light['EntityComponentLight']['emergencyState'].get('@presetTag')
            
            ProType=FOV*(Focal_Beam/1)
                
            if presettag == 'default':
                DecayType=3
            elif presettag == 'interior_fade':
                DecayType=1
            elif presettag == 'f2b_sent':
                DecayType=1
            elif presettag == 'fast':
                DecayType=2
            elif presettag == 'slow':
                DecayType=3
            elif presettag == 'f2b1':
                DecayType=1
            elif presettag == 'f2b_war':
                DecayType=2
            elif presettag == 'flicker1':
                DecayType=3
            elif presettag == 'Ceiling_lights':
                DecayType=2
            elif presettag == 'show':
                DecayType=2
            else:
                DecayType=3
                
        
           
            if maxDistance > light_radius:
                farAtten_end=maxDistance
                farAtten_start=light_radius
            else:
                    farAtten_start=(light_radius*0.8)
                    farAtten_end=light_radius


            if light['EntityComponentLight']['@lightType'] == 'Projector':
                pymxs.runtime.freeSpot(
                rgb=rt.color(Color_R*255, Color_G*255, Color_B*255), 
                multiplier=intensity, 
                nearAttenStart=0.0, 
                nearAttenEnd=MinDistance,
                farAttenStart=farAtten_start,
                farAttenEnd=farAtten_end,
                useNearAtten=rt.false,
                useFarAtten=rt.true,
                decayRadius=bulb_radius, 
                hotspot=ProType, 
                falloff=FOV, 
                coneShape=2,
                projector=rt.true,
                castShadows=rt.true,
                projectorMap=rt.VRaybitmap(HDRIMapName=((r"D:/Star Citizen Master Texture/")+(TextDDS_TIF)), color_space=0),
                shadowGenerator = rt.VRayShadow(),
                name=light_name,
                attenDecay=3)
                
            elif light['EntityComponentLight']['@lightType'] == 'SoftOmni':
                pymxs.runtime.Omnilight(
                rgb=rt.color(Color_R*255, Color_G*255, Color_B*255), 
                multiplier=intensity, 
                nearAttenStart=0.0, 
                nearAttenEnd=bulb_radius,
                farAttenStart=farAtten_start,
                farAttenEnd=farAtten_end,
                useNearAtten=rt.true,
                useFarAtten=rt.true,                
                decayRadius=bulb_radius,
                castShadows=rt.true,
                shadowGenerator = rt.VRayShadow(),
                name=light_name,
                attenDecay=3)
            else:
                pymxs.runtime.Omnilight(
                rgb=rt.color(Color_R*255, Color_G*255, Color_B*255), 
                multiplier=intensity, 
                nearAttenStart=0.0, 
                nearAttenEnd=MinDistance,
                farAttenStart=farAtten_start,
                farAttenEnd=farAtten_end,
                useNearAtten=rt.false,
                useFarAtten=rt.true,                
                decayRadius=bulb_radius,
                castShadows=rt.true,
                shadowGenerator = rt.VRayShadow(),
                name=light_name,
                attenDecay=3)
KLightLoad.close()
