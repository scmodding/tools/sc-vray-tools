rollout SuffixEndModifier "Suffix Modder 1.5" width:330 height:460
	
(

		label 'intrustions21' "Be sure to use correct Blend/Wear Type" pos: [70,10] width: 340 height: 30 	
		button 'btn_WEAR_EXT' "Wear EXT" pos:[20,60] width:120 height:30 align:#left border:true
		button 'btn_WEAR_INT' "Wear INT" pos:[20,95] width:120 height:30 align:#left border:true
		button 'btn_WEAR_CARGO' "Wear CARGO" pos:[20,130] width:120 height:30 align:#left border:true
		button 'btn_WEAR_BAYS' "Wear BAYS" pos:[20,165] width:120 height:30 align:#left border:true
	
		button 'btn_BLEND_EXT' "Blend EXT" pos:[180,60] width:120 height:30 align:#left border:true
		button 'btn_BLEND_INT' "Blend INT" pos:[180,95] width:120 height:30 align:#left border:true
		button 'btn_BLEND_CARGO' "Blend CARGO" pos:[180,130] width:120 height:30 align:#left border:true
		button 'btn_BLEND_BAYS' "Blend BAYS" pos:[180,165] width:120 height:30 align:#left border:true	
	
		label 'intrustions22' "Does not work with materials in a multimaterial mat" pos: [40,430] width: 340 height: 30 	
		label 'intrustions24' "VRayBlendMtl" pos: [125,40] width: 340 height: 15 	
		label 'intrustions25' "VRayMtl" pos: [60,240] width: 340 height: 15 
		label 'intrustions26' "2Side Glass" pos: [210,240] width: 340 height: 15 		
	
		button 'btn_Single_EXT' "Single EXT" pos:[20,260] width:120 height:30 align:#left border:true
		button 'btn_Single_INT' "Single INT" pos:[20,295] width:120 height:30 align:#left border:true
		button 'btn_Single_CARGO' "Single CARGO" pos:[20,330] width:120 height:30 align:#left border:true
		button 'btn_Single_BAYS' "Single BAYS" pos:[20,365] width:120 height:30 align:#left border:true	
		
		button 'btn_Glass_EXT' "Glass EXT" pos:[180,260] width:120 height:30 align:#left border:true
		button 'btn_Glass_INT' "Glass INT" pos:[180,295] width:120 height:30 align:#left border:true
		button 'btn_Glass_CARGO' "Glass CARGO" pos:[180,330] width:120 height:30 align:#left border:true
		button 'btn_Glass_BAYS' "Glass BAYS" pos:[180,365] width:120 height:30 align:#left border:true	
	
	on btn_Glass_EXT pressed do
	(
		try(
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_EXT"
		meditMaterials[activeMeditSlot].frontMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_EXT"
		meditMaterials[activeMeditSlot].frontMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK_EXT"
		meditMaterials[activeMeditSlot].frontMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR_EXT"	
		)catch(messagebox "Wrong Material Type")			
	)
	
	on btn_Glass_INT pressed do
	(
		try(
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_INT"
		meditMaterials[activeMeditSlot].frontMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_INT"
		meditMaterials[activeMeditSlot].frontMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK_INT" 	
		meditMaterials[activeMeditSlot].frontMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR_INT"
		)catch(messagebox "Wrong Material Type")	
	)
	
	
	on btn_Glass_CARGO pressed do
	(try(
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_CARGO"
		meditMaterials[activeMeditSlot].frontMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_CARGO"
		meditMaterials[activeMeditSlot].frontMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK_CARGO" 	
		meditMaterials[activeMeditSlot].frontMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR_CARGO"
		)catch(messagebox "Wrong Material Type")	
	)
	
	on btn_Glass_BAYS pressed do
	(try(
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_BAYS"
		meditMaterials[activeMeditSlot].frontMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_BAYS"
		meditMaterials[activeMeditSlot].frontMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK_BAYS" 
		meditMaterials[activeMeditSlot].frontMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR_CARGO"
	)catch(messagebox "Wrong Material Type")		
	)
	
	on btn_WEAR_EXT pressed do
	(try(
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_EXT"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_EXT"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK_EXT"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR_EXT" 		
		
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_EXT"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[6].name="DIRT_INNER_MASK_EXT"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[6].name="DIRTCOLOR_EXT" 		
		--Mask Adjustment
		try meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name="Wear_CONTROL_EXT" catch (try meditMaterials[activeMeditSlot].texmap_blend[1].name="Wear_CONTROL_EXT" catch()) 
			)catch(messagebox "Wrong Material Type")	
	)
	
	on btn_WEAR_INT pressed do
	(try(
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_INT"
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_INT"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK_INT"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR_INT" 
		
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_INT"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[6].name="DIRT_INNER_MASK_INT" 
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[6].name="DIRTCOLOR_INT" 
		--Mask Adjustment
		try meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name="Wear_CONTROL_INT" catch (try meditMaterials[activeMeditSlot].texmap_blend[1].name="Wear_CONTROL_INT" catch()) 
			)catch(messagebox "Wrong Material Type")	
	)
	
	on btn_WEAR_CARGO pressed do
	(try(
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_CARGO"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_CARGO"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK_CARGO" 
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR_CARGO" 
		
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_CARGO"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[6].name="DIRT_INNER_MASK_CARGO" 
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[6].name="DIRTCOLOR_CARGO" 
		--Mask Adjustment
		try meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name="Wear_CONTROL_CARGO" catch (try meditMaterials[activeMeditSlot].texmap_blend[1].name="Wear_CONTROL_CARGO" catch()) 
		)catch(messagebox "Wrong Material Type")	
	)
	
	on btn_WEAR_BAYS pressed do
	(try(
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_BAYS"
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_BAYS"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK_BAYS"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR_BAYS" 		
		
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_BAYS"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[6].name="DIRT_INNER_MASK_BAYS"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[6].name="DIRTCOLOR_BAYS" 		
		--Mask Adjustment
		try meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name="Wear_CONTROL_BAYS" catch (try meditMaterials[activeMeditSlot].texmap_blend[1].name="Wear_CONTROL_BAYS" catch()) 
			
			)catch(messagebox "Wrong Material Type")	
	)
	
	on btn_BLEND_EXT pressed do
	(try(
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_EXT"
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_EXT"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK_EXT"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR_EXT" 		
		
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_EXT"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[6].name="DIRT_INNER_MASK_EXT"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[6].name="DIRTCOLOR_EXT" 	
		--Mask Adjustment
		try meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name="Blend_CONTROL_EXT" catch (try meditMaterials[activeMeditSlot].texmap_blend[1].name="Blend_CONTROL_EXT" catch()) 
			)catch(messagebox "Wrong Material Type")	
	)
	
	on btn_BLEND_INT pressed do
	(try(
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_INT"
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_INT"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK_INT"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR_INT" 
		
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_INT"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[6].name="DIRT_INNER_MASK_INT" 
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[6].name="DIRTCOLOR_INT" 
		--Mask Adjustment
		try meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name="Blend_CONTROL_INT" catch (try meditMaterials[activeMeditSlot].texmap_blend[1].name="Blend_CONTROL_INTT" catch()) 
			)catch(messagebox "Wrong Material Type")	
	)
	
	on btn_BLEND_CARGO pressed do
	(try(
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_CARGO"
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_CARGO"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_CARGO"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK_CARGO" 
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR_CARGO" 
		
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_CARGO"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[6].name="DIRT_INNER_MASK_CARGO" 
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[6].name="DIRTCOLOR_CARGO" 
		--Mask Adjustment
		try meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name="Blend_CONTROL_CARGO" catch (try meditMaterials[activeMeditSlot].texmap_blend[1].name="Blend_CONTROL_CARGO" catch()) 
			)catch(messagebox "Wrong Material Type")	
	)
	
	on btn_BLEND_BAYS pressed do
	(try(
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_BAYS"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_BAYS"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mask[6].name="DIRT_INNER_MASK_BAYS"
		meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[6].name="DIRTCOLOR_BAYS" 		
		
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_BAYS"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mask[6].name="DIRT_INNER_MASK_BAYS"
		meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[6].name="DIRTCOLOR_BAYS" 	
		--Mask Adjustment
		try meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name="Blend_CONTROL_BAYS" catch (try meditMaterials[activeMeditSlot].texmap_blend[1].name="Blend_CONTROL_BAYS" catch()) 
			)catch(messagebox "Wrong Material Type")	
	)
	--Singles
		on btn_Single_EXT pressed do
	(
		try(
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_EXT"
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_EXT"
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].name="DIRT_INNER_MASK_EXT"
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[6].name="DIRTCOLOR_EXT" 
	)catch(messagebox "Wrong Material Type")				

	)
		on btn_Single_INT pressed do
	(try(
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_INT"
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_INT"
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].name="DIRT_INNER_MASK_INT" 
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[6].name="DIRTCOLOR_INT" 	
		)catch(messagebox "Wrong Material Type")	
	)
	
		on btn_Single_CARGO pressed do
	(try(
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_CARGO"
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_CARGO"
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].name="DIRT_INNER_MASK_CARGO" 
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[6].name="DIRTCOLOR_CARGO" 
		)catch(messagebox "Wrong Material Type")			
	)
		on btn_Single_BAYS pressed do
	(try(
		--Dirt Adjustment
		meditMaterials[activeMeditSlot].name=meditMaterials[activeMeditSlot].name+"_BAYS"
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[7].name = "EDGE_DIRT_MASK_BAYS"
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[6].name="DIRT_INNER_MASK_BAYS" 
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[6].name="DIRTCOLOR_BAYS" 	
		
		)catch(messagebox "Wrong Material Type")	
	)	
	
)

createDialog SuffixEndModifier
