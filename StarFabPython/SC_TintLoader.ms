rollout SC_TintLoader "Tint Loader 0.0.2" width:409 height:257
(
	button 'LoadBtn' "Load" pos:[142,41] width:90 height:18 align:#left
	label 'lbl1' "Tint Loader" pos:[158,15] width:58 height:18 align:#left
	button 'MatBtn' "Load into Mat Slots" pos:[50,166] width:118 height:23 align:#left
	button 'DecalBtn' "Load Decal" pos:[215,166] width:118 height:23 align:#left
	colorPicker 'Tint_1_Picker' "" pos:[38,85] width:69 height:54 align:#left
	colorPicker 'Tint_2_Picker' "" pos:[153,85] width:69 height:54 color:(color 0 0 155) align:#left
	colorPicker 'Tint_3_Picker' "" pos:[266,85] width:69 height:54 color:(color 0 0 155) align:#left
	label 'lbl2' "Tint 1" pos:[58,60] width:32 height:15 align:#left
	label 'lbl4' "Tint 2" pos:[177,60] width:32 height:15 align:#left
	label 'lbl5' "Tint 3" pos:[286,60] width:32 height:15 align:#left
	label 'lbl6' "Preview is not sRGB corrected" pos:[115,142] width:148 height:22 align:#left
	editText 'TexturePath' "Texture Dir" pos:[16,200] width:250 height:24 align:#left text: "D:/Star Citizen Master Texture/"--SET THIS TO YOUR TEXTURE PATH
	edittext 'NameAdjust' "Name Additive" pos:[100,230] width:100 height:24 align:#left
	button 'TintID' "Tint ID" pos:[270,200] width:118 height:23 align:#left
	
	on LoadBtn pressed do
	(
		Import_Instruction= getOpenFileName caption:"Pick TintPallette:" 
		
		
		TintGet_Maker=openfile "T:/SC_Cache/TintInput.txt" mode:"wt" --SET THIS TO TEMP LOCATION
		print (Import_Instruction) to:TintGet_Maker
		close TintGet_Maker
	
		try python.ExecuteFile "R:/Program Files/AutoDesk/3ds Max 2022/scripts/SC_MTL_JSON_TintLoader.py" catch (messageBox "Invalid Tint")                --SET THIS TO YOUR SCRIPT LOCATION
		
		mtl_in_file = openFile "T://SC_Cache//TintOutput.txt" mode: "rt" --SET THIS TO TEMP LOCATION
		
		DecalColorR_R=execute(readline mtl_in_file) as float
		DecalColorR_G=execute(readline mtl_in_file) as float
		DecalColorR_B=execute(readline mtl_in_file) as float
		
		DecalColorG_R=execute(readline mtl_in_file) as float
		DecalColorG_G=execute(readline mtl_in_file) as float
		DecalColorG_B=execute(readline mtl_in_file) as float
		
		DecalColorB_R=execute(readline mtl_in_file) as float
		DecalColorB_G=execute(readline mtl_in_file) as float
		DecalColorB_B=execute(readline mtl_in_file) as float
		
		DecalText=execute(readline mtl_in_file) as string
		
		Tint1_C_R=execute(readline mtl_in_file) as float
		Tint1_C_G=execute(readline mtl_in_file) as float
		Tint1_C_B=execute(readline mtl_in_file) as float
		
		Tint1_S_R=execute(readline mtl_in_file) as float
		Tint1_S_G=execute(readline mtl_in_file) as float
		Tint1_S_B=execute(readline mtl_in_file) as float
		
		Tint1_G=execute(readline mtl_in_file) as float
		
		Tint2_C_R=execute(readline mtl_in_file) as float
		Tint2_C_G=execute(readline mtl_in_file) as float
		Tint2_C_B=execute(readline mtl_in_file) as float
		
		Tint2_S_R=execute(readline mtl_in_file) as float
		Tint2_S_G=execute(readline mtl_in_file) as float
		Tint2_S_B=execute(readline mtl_in_file) as float
		
		Tint2_G=execute(readline mtl_in_file) as float
		
		Tint3_C_R=execute(readline mtl_in_file) as float
		Tint3_C_G=execute(readline mtl_in_file) as float
		Tint3_C_B=execute(readline mtl_in_file) as float
		
		Tint3_S_R=execute(readline mtl_in_file) as float
		Tint3_S_G=execute(readline mtl_in_file) as float
		Tint3_S_B=execute(readline mtl_in_file) as float
		
		Tint3_G=execute(readline mtl_in_file) as float
		
		GlassColor_R=execute(readline mtl_in_file) as float
		GlassColor_G=execute(readline mtl_in_file) as float
		GlassColor_B=execute(readline mtl_in_file) as float
		

				
		Tint_1_Picker.color=color (Tint1_C_R) (Tint1_C_G) (Tint1_C_B) 255
		Tint_2_Picker.color=color (Tint2_C_R) (Tint2_C_G) (Tint2_C_B) 255
		Tint_3_Picker.color=color (Tint3_C_R) (Tint3_C_G) (Tint3_C_B) 255
		

		
		close mtl_in_file

	)
	
	on MatBtn pressed do
	(
		mtl_in_file = openFile "T://SC_Cache//TintOutput.txt" mode: "rt" --SET THIS TO TEMP LOCATION
		
		DecalColorR_R=execute(readline mtl_in_file) as float
		DecalColorR_G=execute(readline mtl_in_file) as float
		DecalColorR_B=execute(readline mtl_in_file) as float
		
		DecalColorG_R=execute(readline mtl_in_file) as float
		DecalColorG_G=execute(readline mtl_in_file) as float
		DecalColorG_B=execute(readline mtl_in_file) as float
		
		DecalColorB_R=execute(readline mtl_in_file) as float
		DecalColorB_G=execute(readline mtl_in_file) as float
		DecalColorB_B=execute(readline mtl_in_file) as float
		
		DecalText=execute(readline mtl_in_file) as string
		
		Tint1_C_R=execute(readline mtl_in_file) as float
		Tint1_C_G=execute(readline mtl_in_file) as float
		Tint1_C_B=execute(readline mtl_in_file) as float
		
		Tint1_S_R=execute(readline mtl_in_file) as float
		Tint1_S_G=execute(readline mtl_in_file) as float
		Tint1_S_B=execute(readline mtl_in_file) as float
		
		Tint1_G=execute(readline mtl_in_file) as float
		
		Tint2_C_R=execute(readline mtl_in_file) as float
		Tint2_C_G=execute(readline mtl_in_file) as float
		Tint2_C_B=execute(readline mtl_in_file) as float
		
		Tint2_S_R=execute(readline mtl_in_file) as float
		Tint2_S_G=execute(readline mtl_in_file) as float
		Tint2_S_B=execute(readline mtl_in_file) as float
		
		Tint2_G=execute(readline mtl_in_file) as float
		
		Tint3_C_R=execute(readline mtl_in_file) as float
		Tint3_C_G=execute(readline mtl_in_file) as float
		Tint3_C_B=execute(readline mtl_in_file) as float
		
		Tint3_S_R=execute(readline mtl_in_file) as float
		Tint3_S_G=execute(readline mtl_in_file) as float
		Tint3_S_B=execute(readline mtl_in_file) as float
		
		Tint3_G=execute(readline mtl_in_file) as float
		
		GlassColor_R=execute(readline mtl_in_file) as float
		GlassColor_G=execute(readline mtl_in_file) as float
		GlassColor_B=execute(readline mtl_in_file) as float
	
		

		
		meditMaterials[activeMeditSlot]=VRayMtl()
		meditMaterials[activeMeditSlot].texmap_diffuse= CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].color=color (Tint1_C_R) (Tint1_C_G) (Tint1_C_B) 255
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].gammaRGB = (1 / 2.2)
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].name="TINT_1"
		
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].color=color (Tint2_C_R) (Tint2_C_G) (Tint2_C_B) 255
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].gammaRGB = (1 / 2.2)
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].name="TINT_2"
		
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].color=color (Tint3_C_R) (Tint3_C_G) (Tint3_C_B) 255
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].gammaRGB = (1 / 2.2)
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].name="TINT_3"
		
		---gloss
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness= CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].color=color (Tint1_G) (Tint1_G) (Tint1_G) 255
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].gammaRGB = (2.2)
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].name="TINT_GLOSS_1"		

		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[2].color=color (Tint2_G) (Tint2_G) (Tint2_G) 255
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[2].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[2].gammaRGB = (2.2)
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[2].name="TINT_GLOSS_2"		

		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[3].color=color (Tint3_G) (Tint3_G) (Tint3_G) 255
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[3].gammaRGB = (2.2)
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[3].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[3].name="TINT_GLOSS_3"
		--spec
		meditMaterials[activeMeditSlot].texmap_reflection= CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].color=color (Tint1_S_R) (Tint1_S_G) (Tint1_S_B) 255
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].gammaRGB = (2.2)
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].name="TINT_SPEC_1"
		
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].color=color (Tint2_S_R) (Tint2_S_G) (Tint2_S_B) 255
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].gammaRGB = (2.2)
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].name="TINT_SPEC_2"
		
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3].color=color (Tint3_S_R) (Tint3_S_G) (Tint3_S_B) 255
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3].gammaRGB = (2.2)
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3].name="TINT_SPEC_3"
		
		if NameAdjust.text != "" do
		(
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].name=meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].name+NameAdjust.text
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].name=meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].name+NameAdjust.text	
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].name=meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].name+NameAdjust.text
			
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].name=meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].name+NameAdjust.text
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[2].name=meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[2].name+NameAdjust.text	
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[3].name=meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[3].name+NameAdjust.text
			
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].name=meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].name+NameAdjust.text
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].name=meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].name+NameAdjust.text	
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3].name=meditMaterials[activeMeditSlot].texmap_reflection.maplist[3].name+NameAdjust.text

			
		)
		
		meditMaterials[activeMeditSlot].texmap_refraction_fog= Color_Correction ()
		meditMaterials[activeMeditSlot].texmap_refraction_fog.name="Glass FOG Color"
		meditMaterials[activeMeditSlot].texmap_refraction_fog.lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_refraction_fog.gammaRGB = (2.2)
		meditMaterials[activeMeditSlot].texmap_refraction_fog.color=color (GlassColor_R) (GlassColor_G) (GlassColor_B) 255
		
		
		close mtl_in_file
	)
	
	on DecalBtn pressed do
	(
		mtl_in_file = openFile "T://SC_Cache//TintOutput.txt" mode: "rt" --SET THIS TO TEMP LOCATION
		
		DecalColorR_R=execute(readline mtl_in_file) as float
		DecalColorR_G=execute(readline mtl_in_file) as float
		DecalColorR_B=execute(readline mtl_in_file) as float
		
		DecalColorG_R=execute(readline mtl_in_file) as float
		DecalColorG_G=execute(readline mtl_in_file) as float
		DecalColorG_B=execute(readline mtl_in_file) as float
		
		DecalColorB_R=execute(readline mtl_in_file) as float
		DecalColorB_G=execute(readline mtl_in_file) as float
		DecalColorB_B=execute(readline mtl_in_file) as float
		
		DecalText=execute(readline mtl_in_file) as string
		
		Tint1_C_R=execute(readline mtl_in_file) as float
		Tint1_C_G=execute(readline mtl_in_file) as float
		Tint1_C_B=execute(readline mtl_in_file) as float
		
		Tint1_S_R=execute(readline mtl_in_file) as float
		Tint1_S_G=execute(readline mtl_in_file) as float
		Tint1_S_B=execute(readline mtl_in_file) as float
		
		Tint1_G=execute(readline mtl_in_file) as float
		
		Tint2_C_R=execute(readline mtl_in_file) as float
		Tint2_C_G=execute(readline mtl_in_file) as float
		Tint2_C_B=execute(readline mtl_in_file) as float
		
		Tint2_S_R=execute(readline mtl_in_file) as float
		Tint2_S_G=execute(readline mtl_in_file) as float
		Tint2_S_B=execute(readline mtl_in_file) as float
		
		Tint2_G=execute(readline mtl_in_file) as float
		
		Tint3_C_R=execute(readline mtl_in_file) as float
		Tint3_C_G=execute(readline mtl_in_file) as float
		Tint3_C_B=execute(readline mtl_in_file) as float
		
		Tint3_S_R=execute(readline mtl_in_file) as float
		Tint3_S_G=execute(readline mtl_in_file) as float
		Tint3_S_B=execute(readline mtl_in_file) as float
		
		Tint3_G=execute(readline mtl_in_file) as float
		
		GlassColor_R=execute(readline mtl_in_file) as float
		GlassColor_G=execute(readline mtl_in_file) as float
		GlassColor_B=execute(readline mtl_in_file) as float
		
		meditMaterials[activeMeditSlot]=VRayMtl()
		meditMaterials[activeMeditSlot].name="TINT_DECAL"
		meditMaterials[activeMeditSlot].option_opacityMode = 1
		meditMaterials[activeMeditSlot].texmap_diffuse= CompositeTexturemap()
		--black
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].color=color 0 0 0 255
		--red
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2]=CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].rewireR=0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].rewireG=10
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].rewireB=10
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].map.HDRIMapName=(TexturePath.text+DecalText)
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].map.color_space=0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].blendMode[1]=0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].blendMode[2]=23
		--green
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].rewireR=10
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].rewireG=1
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].rewireB=10
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].map.HDRIMapName=meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].map.HDRIMapName
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[1].map.color_space=0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].blendMode[1]=0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[3].blendMode[2]=23	
		--blue
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].rewireR=10
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].rewireG=10
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].rewireB=2
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].map.HDRIMapName=meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].map.HDRIMapName
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[1].map.color_space=0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].blendMode[1]=0
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_diffuse.mask[4].blendMode[2]=23

		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].color=color (DecalColorR_R)(DecalColorR_G)(DecalColorR_B) 255
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].gammaRGB = (1 / 2.2)
		
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].color=color (DecalColorG_R)(DecalColorG_G)(DecalColorG_B) 255
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].gammaRGB = (1 / 2.2)
		
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[4]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[4].color=color (DecalColorB_R)(DecalColorB_G)(DecalColorB_B) 255
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[4].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[4].gammaRGB = (1 / 2.2)
		
		--opac
		meditMaterials[activeMeditSlot].texmap_opacity=CompositeTextureMap()		
		--red
		meditMaterials[activeMeditSlot].texmap_opacity.mask[1]=CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[1].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[1].maplist[1].rewireR=0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[1].maplist[1].rewireG=10
		meditMaterials[activeMeditSlot].texmap_opacity.mask[1].maplist[1].rewireB=10
		meditMaterials[activeMeditSlot].texmap_opacity.mask[1].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_opacity.mask[1].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[1].maplist[1].map.HDRIMapName=meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].map.HDRIMapName
		meditMaterials[activeMeditSlot].texmap_opacity.mask[1].maplist[1].map.color_space=0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[1].blendMode[1]=0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[1].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[1].blendMode[2]=23
		--green
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].rewireR=10
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].rewireG=1
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].rewireB=10
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].map.HDRIMapName=meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].map.HDRIMapName
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[1].map.color_space=0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].blendMode[1]=0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[2].blendMode[2]=23	
		--blue
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3]=CompositeTextureMap()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].rewireR=10
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].rewireG=10
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].rewireB=2
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].rewireA=9
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].map=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].map.HDRIMapName=meditMaterials[activeMeditSlot].texmap_diffuse.mask[2].maplist[1].map.HDRIMapName
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[1].map.color_space=0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].blendMode[1]=0
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].maplist[2]=RGB_Tint()
		meditMaterials[activeMeditSlot].texmap_opacity.mask[3].blendMode[2]=23

		meditMaterials[activeMeditSlot].texmap_opacity.maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].color=color 255 255 255 255

		meditMaterials[activeMeditSlot].texmap_opacity.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_opacity.maplist[2].color=color 255 255 255 255

		
		meditMaterials[activeMeditSlot].texmap_opacity.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_opacity.maplist[3].color=color 255 255 255 255

		meditMaterials[activeMeditSlot].Reflection = color 8 8 8
		meditMaterials[activeMeditSlot].reflection_glossiness = 0.91


		close mtl_in_file
	)
	
	on TintID pressed do
	(
		meditMaterials[activeMeditSlot]=VRayMtl()
		meditMaterials[activeMeditSlot].texmap_diffuse= CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].color=color 255 0 255 255
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].gammaRGB = (1 / 2.2)
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].name="TINT_1"+NameAdjust.text
		
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].color=color 255 0 255 255
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].gammaRGB = (1 / 2.2)
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].name="TINT_2"+NameAdjust.text
		
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].color=color 255 0 255 255
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].gammaRGB = (1 / 2.2)
		meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].name="TINT_3"+NameAdjust.text
		
		---gloss
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness= CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].color=color 255 0 255 255
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].name="TINT_GLOSS_1"+NameAdjust.text
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].gammaRGB = (2.2)		

		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[2].color=color 255 0 255 255
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[2].name="TINT_GLOSS_2"+NameAdjust.text
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[2].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[2].gammaRGB = (2.2)				

		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[3].color=color 255 0 255 255
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[3].name="TINT_GLOSS_3"+NameAdjust.text
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[3].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[3].gammaRGB = (2.2)				
		--spec
		meditMaterials[activeMeditSlot].texmap_reflection= CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].color=color 255 0 255 255
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].gammaRGB = (2.2)
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].name="TINT_SPEC_1"+NameAdjust.text
		
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].color=color 255 0 255 255
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].gammaRGB = (2.2)
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].name="TINT_SPEC_2"+NameAdjust.text
		
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3].color=color 255 0 255 255
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3].lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3].gammaRGB = (2.2)
		meditMaterials[activeMeditSlot].texmap_reflection.maplist[3].name="TINT_SPEC_3"+NameAdjust.text	

		meditMaterials[activeMeditSlot].texmap_refraction_fog= Color_Correction ()
		meditMaterials[activeMeditSlot].texmap_refraction_fog.name="Glass FOG Color"
		meditMaterials[activeMeditSlot].texmap_refraction_fog.lightnessMode = 1
		meditMaterials[activeMeditSlot].texmap_refraction_fog.gammaRGB = (2.2)
		meditMaterials[activeMeditSlot].texmap_refraction_fog.color=color 255 0 255 255
	)
)

createDialog SC_TintLoader 
