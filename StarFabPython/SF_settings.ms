maxINI = getMAXIniFile()
tex_path = getINISetting maxINI "SCTools" "tex_path"
cache_path = getINISetting maxINI "SCTools" "cache_path"
script_path = getINISetting maxINI "SCTools" "script_path"
json_path = getINISetting maxINI "SCTools" "json_path"
Mat_path = getINISetting maxINI "SCTools" "Mat_path"
Light_path = getINISetting maxINI "SCTools" "Light_path"

rollout settings "StarFab 3ds Max Path Settings by RobotSpartan/modify by Baconator" width:480 height:350
(

	editText tex_path_lbl "Texture Path" pos:[35,37] width:350 height:17 align:#left text:tex_path labelOnTop: true readOnly: true
	button modify_tex "Modify" pos:[400,54] width:40 height:17 align:#left
	
	editText cache_path_lbl "Cache Path" pos:[35,82] width:350 height:17 align:#left text:cache_path labelOnTop: true readOnly: true
	button modify_cache "Modify" pos:[400,100] width:40 height:17 align:#left
	
	editText scripts_path_lbl "Scripts Path" pos:[35,127] width:350 height:17 align:#left text:script_path labelOnTop: true readOnly: true
	button modify_scripts "Modify" pos:[400,145] width:40 height:17 align:#left
	
	editText json_path_lbl "JSON Path" pos:[35,172] width:350 height:17 align:#left text:json_path labelOnTop: true readOnly: true
	button modify_json "Modify" pos:[400,190] width:40 height:17 align:#left
	
	editText MatDir_path_lbl "Mat Path" pos:[35,217] width:350 height:17 align:#left text:Mat_path labelOnTop: true readOnly: true
	button modify_MatDir "Modify" pos:[400,235] width:40 height:17 align:#left
	
	editText Light_path_lbl "Light Path" pos:[35,262] width:350 height:17 align:#left text:Light_path labelOnTop: true readOnly: true
	button modify_Light "Modify" pos:[400,280] width:40 height:17 align:#left
	
	button save "Save" pos:[150,310] width:60 height:37 align:#left
	button close "Close" pos:[250,310] width:60 height:37 align:#left
	
	label lbl4 "Path Settings" pos:[21,5] width:299 height:26 align:#left
	
	on modify_tex pressed do
	(
		tex_path_lbl.readOnly = false
	)

	on modify_cache pressed do
	(
		cache_path_lbl.readOnly = false
	)
	on modify_scripts pressed do
	(
		scripts_path_lbl.readOnly = false
	)
	on modify_json pressed do
	(
		json_path_lbl.readOnly = false
	)
	on modify_MatDir pressed do
	(
		MatDir_path_lbl.readOnly = false
	)
	
	on modify_Light pressed do
	(
		Light_path_lbl.readOnly = false
	)
	--
	on tex_path_lbl changed arg_tex do
	(
		tex_path = arg_tex
	)
	on cache_path_lbl changed arg_cache do
	(
		cache_path = arg_cache
	)
	on scripts_path_lbl changed arg_scripts do
	(
		script_path = arg_scripts
	)
	on json_path_lbl changed arg_json do
	(
		json_path = arg_json
	)
	
	on Light_path_lbl changed arg_Mat do
	(
		Light_path = arg_Mat
	)
	
	on save pressed do
	(
	setINISetting maxINI "SCTools" "tex_path" tex_path
	setINISetting maxINI "SCTools" "cache_path" cache_path
	setINISetting maxINI "SCTools" "script_path" script_path
	setINISetting maxINI "SCTools" "json_path" json_path
	setINISetting maxINI "SCTools" "Mat_path" Mat_path
	setINISetting maxINI "SCTools" "Light_path" Light_path
		
	messagebox "Paths Set"
	)
	
	on close pressed do
	
	(
	DestroyDialog settings
	)
	
)
CreateDialog settings 
